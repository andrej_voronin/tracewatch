from twisted.spread import pb
from twisted.web import xmlrpc

class StoragePbResource(pb.Root):
    def __init__(self, imp):
        self.imp = imp
    def remote_getLocationTrack(self, equip_id, timestamp_from, timestamp_to):
        return self.imp.getLocationTrack(equip_id, timestamp_from, timestamp_to)

    def remote_getCounters(self):
        return self.imp.getCounters()

    def remote_processLogDb(self):
        return self.imp.processLogDb()

    def remote_createTrackView(self, serial, timestamp_from, timestamp_to):
        return self.imp.createTrackView(serial, timestamp_from, timestamp_to)

    def remote_getTrackViewPage(self, view_id, page_n):
        return self.imp.getTrackViewPage(view_id, page_n)

class StorageXmlrpcResource(xmlrpc.XMLRPC):
    def __init__(self, imp):
        xmlrpc.XMLRPC.__init__(self)
        self.imp = imp

    def xmlrpc_getLocationTrack(self, equip_id, timestamp_from, timestamp_to):
        return self.imp.getLocationTrack(equip_id, timestamp_from, timestamp_to)

    def xmlrpc_getCounters(self):
        return self.imp.getCounters()

    def xmlrpc_processLogDb(self):
        return self.imp.processLogDb()

    def xmlrpc_createTrackView(self, serial, timestamp_from, timestamp_to):
        return self.imp.createTrackView(serial, timestamp_from, timestamp_to)

    def xmlrpc_getTrackViewPage(self, view_id, page_n):
        return self.imp.getTrackViewPage(view_id, page_n)

