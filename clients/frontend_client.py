from twisted.internet import reactor
from twisted.application import service
from twisted.python import log
from twisted.spread import pb
from twisted.internet import task

import time

class FrontendClientService(service.Service, pb.Referenceable):
    def __init__(self, h, p):
        self.root = None
        self.m = None        
        self._step = 5
        self.h = h
        self.p = p
        
    def onRootDisconnect(self, root):
        log.msg("FrontendClient: root disconnected")                
        self.root = None
        self.m = None
        if not hasattr(self, '_loop'):
            self._loop = task.LoopingCall(self._pulse)
            self._loop.start(self._step, now=False).addErrback(self._failed)             
            
    def getRootObjectSuccess(self, root):
        self.root = root
        self.root.notifyOnDisconnect(self.onRootDisconnect)
        self.root.callRemote("Advise", self).addCallback(self.adviseSuccess).addErrback(self.adviseError)
        
    def getRootObjectError(self, reason):
        log.msg("FrontendClient: root object getting error: %s" % (reason))     
        if not hasattr(self, '_loop'):
            self._loop = task.LoopingCall(self._pulse)
            self._loop.start(self._step, now=False).addErrback(self._failed)        
            
    def adviseSuccess(self, m):
        self.m = m   
        log.msg('FrontendClient: advise success')
        if hasattr(self, '_loop'):
            self._loop.stop()
            delattr(self, '_loop')  
                     
    def adviseError(self, reason):
        log.msg("FrontendClient: advise error: %s" % (reason))                
        if not hasattr(self, '_loop'):
            self._loop = task.LoopingCall(self._pulse)
            self._loop.start(self._step, now=False).addErrback(self._failed)
            
    def unadviseSuccess(self):
        log.msg('FrontendClient: unadvise success')
        
    def unadviseError(self, reason):
        log.msg("FrontendClient: unadvise error: %s" % (reason))  
                              
    def _failed(self, why):
        self._loop.running = False
        log.err('FrontendClient: timer service failed:%s' % (why))
        
    def _pulse(self):
        if None != self.root:
            if None == self.m:
                self.root.callRemote("Advise", self).addCallback(self.adviseSuccess).addErrback(self.adviseError)   
        else:
            cf = pb.PBClientFactory()
            cf.getRootObject().addCallback(self.getRootObjectSuccess).addErrback(self.getRootObjectError)                            
            reactor.connectTCP(self.h, self.p, cf)
            
    def startService(self):
        service.Service.startService(self)
        cf = pb.PBClientFactory()
        cf.getRootObject().addCallback(self.getRootObjectSuccess).addErrback(self.getRootObjectError)                            
        reactor.connectTCP(self.h, self.p, cf)
        
    def stopService(self):
        if hasattr(self, '_loop'):
            if self._loop.running:
                self._loop.stop()
        if None != self.root:
            if None != self.m:
                self.root.callRemote("Unadvise", self.m).addCallback(self.unadviseSuccess).addErrback(self.unadviseError)   
        return service.Service.stopService(self)
        
    def stateReport(self, serial, ts, j):
        pass
        
    def remote_stateReport(self, serial, ts, j):
        self.stateReport(serial, ts, j)




