from twisted.python import log
from twisted.spread import pb
from twisted.web import xmlrpc
from twisted.internet import defer

import datetime
import time
import sqlite3
import cPickle
import os
import types

try:
    import json
except ImportError:
    import simplejson as json

from frontend_client import FrontendClientService
from lineguard.services.lineguard import extractLineguardFields

def createTables(dbcon):
    create_exprs = ['''
CREATE TABLE [dbo].[Gates](
    [Gate_ID] [int] IDENTITY(1,1) NOT NULL,
    [Gate_Code] [nvarchar](250) NOT NULL,
    [Gate_Activity] [int] NOT NULL,
    [Gate_Host] [nvarchar](250) NOT NULL,
    [Gate_Port] [int] NOT NULL,
    [Gate_Address] [nvarchar](2000) NOT NULL,
    [Gate_SendQueue] [nvarchar](2000) NOT NULL,
    [Gate_ReceiveQueue] [nvarchar](2000) NOT NULL,
    [Gate_Info] [nvarchar](2000) NULL,
 CONSTRAINT [PK_Gates] PRIMARY KEY CLUSTERED
(
    [Gate_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF_Gates_Gate_Code]  DEFAULT (newid()) FOR [Gate_Code]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF__Gates__Gate_Acti__7C8480AE]  DEFAULT ((1)) FOR [Gate_Activity]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF_Gates_Gate_Host]  DEFAULT (N'127.0.0.1') FOR [Gate_Host]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF_Gates_Gate_Port]  DEFAULT ((7005)) FOR [Gate_Port]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF_Gates_Gate_Address]  DEFAULT (N'UNI://KBNV/KONDOR/GATE.DEFAULT') FOR [Gate_Address]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF__Gates__Gate_Send__7D78A4E7]  DEFAULT (N'') FOR [Gate_SendQueue]
''',
'''
ALTER TABLE [dbo].[Gates] ADD  CONSTRAINT [DF__Gates__Gate_Rece__7E6CC920]  DEFAULT (N'') FOR [Gate_ReceiveQueue]
''',
'''
CREATE TABLE [dbo].[Sources](
    [Source_ID] [int] IDENTITY(1,1) NOT NULL,
    [Source_Code] [nvarchar](250) NOT NULL,
    [Source_SerialNumber] [int] NOT NULL,
    [Source_Model] [nvarchar](32) NOT NULL,
    [Source_SubModel] [nvarchar](32) NOT NULL,
    [Source_Version] [nvarchar](32) NOT NULL,
    [Source_DateTimeRegistration] [datetime] NOT NULL,
    [Source_Info] [nvarchar](2000) NULL,
    [Source_Activity] [int] NOT NULL,
 CONSTRAINT [PK_Sources] PRIMARY KEY CLUSTERED
(
    [Source_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[Sources] ADD  DEFAULT (N'LG2310') FOR [Source_Model]
''',
'''
ALTER TABLE [dbo].[Sources] ADD  DEFAULT (N'E1') FOR [Source_SubModel]
''',
'''
ALTER TABLE [dbo].[Sources] ADD  DEFAULT (N'') FOR [Source_Version]
''',
'''
ALTER TABLE [dbo].[Sources] ADD  DEFAULT (getutcdate()) FOR [Source_DateTimeRegistration]
''',
'''
ALTER TABLE [dbo].[Sources] ADD  DEFAULT ((1)) FOR [Source_Activity]
''',
'''
CREATE FUNCTION [dbo].[Sources_GetID]
(
    @p_SerialNumber int
)
RETURNS int
AS
BEGIN
    DECLARE @Result int

    SELECT @Result = Source_ID From  dbo.Sources Where  Source_SerialNumber =  @p_SerialNumber

    RETURN @Result
END
''',
'''
CREATE TABLE [dbo].[Destinations](
    [Destination_ID] [int] IDENTITY(1,1) NOT NULL,
    [Destionation_Address] [nvarchar](32) NOT NULL,
    [Destionation_TypeCode] [nvarchar](32) NULL,
    [Destination_Code] [nvarchar](250) NULL,
    [Destination_Info] [nvarchar](2000) NULL,
    [Destination_DateTimeRegistration] [datetime] NOT NULL,
    [Destination_ProcedureCode] [nvarchar](250) NULL,
    [Gate_ID] [int] NULL,
 CONSTRAINT [PK_Destinations] PRIMARY KEY CLUSTERED
(
    [Destination_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[Destinations]  WITH CHECK ADD  CONSTRAINT [FK_Destinations_Gates] FOREIGN KEY([Gate_ID])
REFERENCES [dbo].[Gates] ([Gate_ID])
''',
'''
ALTER TABLE [dbo].[Destinations] CHECK CONSTRAINT [FK_Destinations_Gates]
''',
'''
ALTER TABLE [dbo].[Destinations] ADD  CONSTRAINT [DF__Destinati__Desti__060DEAE8]  DEFAULT (getutcdate()) FOR [Destination_DateTimeRegistration]
''',
'''
CREATE TABLE [dbo].[SnapShots](
    [SnapShot_ID] [int] IDENTITY(1,1) NOT NULL,
    [SnapShot_GUID] [nvarchar](48) NOT NULL,
    [Gate_ID] [int] NULL,
    [Source_ID] [int] NULL,
    [Destination_ID] [int] NULL,
    [SnapShot_DateTime] [datetime] NOT NULL,
    [SnapShot_DateTimeReceive] [datetime] NOT NULL,
    [SnapShot_Direction] [nvarchar](32) NOT NULL,
    [SnapShot_Channel] [nvarchar](32) NOT NULL,
    [SnapShot_Entity] [nvarchar](32) NOT NULL,
    [SnapShot_SubEntity] [nvarchar](32) NULL,
    [SnapShot_ReTranslator] [int] NOT NULL,
    CONSTRAINT [PK_SnapShots] PRIMARY KEY CLUSTERED
    (
        [SnapShot_ID] ASC
    ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[SnapShots]  WITH CHECK ADD  CONSTRAINT [FK_SnapShots_Destinations] FOREIGN KEY([Gate_ID])
REFERENCES [dbo].[Gates] ([Gate_ID])
''',
'''
ALTER TABLE [dbo].[SnapShots] CHECK CONSTRAINT [FK_SnapShots_Destinations]
''',
'''
ALTER TABLE [dbo].[SnapShots]  WITH CHECK ADD  CONSTRAINT [FK_SnapShots_Gates] FOREIGN KEY([Gate_ID])
REFERENCES [dbo].[Gates] ([Gate_ID])
''',
'''
ALTER TABLE [dbo].[SnapShots] CHECK CONSTRAINT [FK_SnapShots_Gates]
''',
'''
ALTER TABLE [dbo].[SnapShots]  WITH CHECK ADD  CONSTRAINT [FK_SnapShots_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[SnapShots] CHECK CONSTRAINT [FK_SnapShots_Sources]
''',
'''
ALTER TABLE [dbo].[SnapShots] ADD  CONSTRAINT [DF_SnapShots_SnapShot_DateTimeReceive]  DEFAULT (getutcdate()) FOR [SnapShot_DateTimeReceive]
''',
'''
ALTER TABLE [dbo].[SnapShots] ADD  CONSTRAINT [DF__SnapShots__SnapS__07F6335A]  DEFAULT ((0)) FOR [SnapShot_ReTranslator]
''',
'''
CREATE PROCEDURE [dbo].[SnapShots_Write]
    @p_ShotGUID         nvarchar(48),
    @p_GateCode         nvarchar(250),
    @p_SourceCode       nvarchar(250),
    @p_DestinationCode  nvarchar(250),
    @p_ShotTime         datetime,
    @p_ReceiveTime      datetime,
    @p_Direction        nvarchar(32),
    @p_Channel          nvarchar(32),
    @p_Entity           nvarchar(32),
    @p_SubEntity        nvarchar(32),
    @p_ReTranslation    int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @ResultCode         int
    Declare @GateID             int
    Declare @SourceID           int
    Declare @DestinationID      int

    Select @ResultCode = 0
    Select @GateID = Gate_ID From dbo.Gates Where Gate_Code = upper(@p_GateCode) and Gate_Activity = 1
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    Select @DestinationID = Destination_ID From dbo.Destinations Where Destination_Code = upper(@p_DestinationCode)

    if @SourceID is not null
        Begin
            /*Begin TransAction*/

                INSERT INTO
                    dbo.SnapShots
                    (
                        [SnapShot_GUID],
                        [Gate_ID],
                        [Source_ID],
                        [Destination_ID],
                        [SnapShot_DateTime],
                        [SnapShot_DateTimeReceive],
                        [SnapShot_Direction],
                        [SnapShot_Channel],
                        [SnapShot_Entity],
                        [SnapShot_SubEntity],
                        [SnapShot_ReTranslator]
                    )
                    VALUES
                        (
                            @p_ShotGUID,
                            @GateID,
                            @SourceID,
                            @DestinationID,
                            @p_ShotTime,
                            @p_ReceiveTime,
                            @p_Direction,
                            @p_Channel,
                            @p_Entity,
                            @p_SubEntity,
                            @p_ReTranslation
                        )
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
                Select @ResultCode = scope_identity()
            /*Commit TransAction*/
        End
    Return @ResultCode
END
''',
'''
CREATE TABLE [dbo].[CurrentSnapShots](
    [CurrentSnapShot_ID] [int] IDENTITY(1,1) NOT NULL,
    [CurrentSnapShot_GUID] [nvarchar](48) NULL,
    [CurrentSnapShot_DateTime] [datetime] NULL,
    [CurrentSnapShot_DateTimeReceive] [datetime] NULL,
    [CurrentSnapShot_Direction] [nvarchar](32) NULL,
    [CurrentSnapShot_Channel] [nvarchar](32) NULL,
    [CurrentSnapShot_Entity] [nvarchar](32) NULL,
    [CurrentSnapShot_ReTranslator] [int] NULL,
    [CurrentSnapShot_SubEntity] [nvarchar](32) NULL,
    [Gate_ID] [int] NULL,
    [Source_ID] [int] NULL,
    [Destination_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
 CONSTRAINT [PK_CurrentSnapShots] PRIMARY KEY CLUSTERED
(
    [CurrentSnapShot_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots]  WITH CHECK ADD  CONSTRAINT [FK_CurrentSnapShots_Destinations] FOREIGN KEY([Destination_ID])
REFERENCES [dbo].[Destinations] ([Destination_ID])
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots] CHECK CONSTRAINT [FK_CurrentSnapShots_Destinations]
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots]  WITH CHECK ADD  CONSTRAINT [FK_CurrentSnapShots_Gates] FOREIGN KEY([Gate_ID])
REFERENCES [dbo].[Gates] ([Gate_ID])
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots] CHECK CONSTRAINT [FK_CurrentSnapShots_Gates]
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots]  WITH CHECK ADD  CONSTRAINT [FK_CurrentSnapShots_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots] CHECK CONSTRAINT [FK_CurrentSnapShots_SnapShots]
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots]  WITH CHECK ADD  CONSTRAINT [FK_CurrentSnapShots_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots] CHECK CONSTRAINT [FK_CurrentSnapShots_Sources]
''',
'''
ALTER TABLE [dbo].[CurrentSnapShots] ADD  CONSTRAINT [DF__CurrentSn__Curre__1BFD2C07]  DEFAULT ((0)) FOR [CurrentSnapShot_ReTranslator]
''',
'''
CREATE PROCEDURE [dbo].[CurrentSnapShots_Write]
    @p_ShotGUID         nvarchar(48),
    @p_GateCode         nvarchar(250),
    @p_SourceCode       nvarchar(250),
    @p_DestinationCode  nvarchar(250),
    @p_ShotTime         datetime,
    @p_ReceiveTime      datetime,
    @p_Direction        nvarchar(32),
    @p_Channel          nvarchar(32),
    @p_Entity           nvarchar(32),
    @p_SubEntity        nvarchar(32),
    @p_ReTranslation    int,
    @p_ShotID           int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @GateID             int
    Declare @SourceID           int
    Declare @DestinationID      int

    Select @GateID = Gate_ID From dbo.Gates Where Gate_Code = upper(@p_GateCode) and Gate_Activity = 1
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    Select @DestinationID = Destination_ID From dbo.Destinations Where Destination_Code = upper(@p_DestinationCode)
    if @SourceID is not null
        Begin
            /*Begin TransAction*/

                Update
                    dbo.CurrentSnapShots
                    Set
                        [CurrentSnapShot_GUID]              =   @p_ShotGUID,
                        [Gate_ID]                           =   @GateID,
                        [Destination_ID]                    =   @DestinationID,
                        [CurrentSnapShot_DateTime]          =   @p_ShotTime,
                        [CurrentSnapShot_DateTimeReceive]   =   @p_ReceiveTime,
                        [CurrentSnapShot_Direction]         =   @p_Direction,
                        [CurrentSnapShot_Channel]           =   @p_Channel,
                        [CurrentSnapShot_Entity]            =   @p_Entity,
                        [CurrentSnapShot_SubEntity]         =   @p_SubEntity,
                        [CurrentSnapShot_ReTranslator]      =   @p_ReTranslation,
                        [SnapShot_ID]                       =   @p_ShotID
                    Where
                        [Source_ID] =   @SourceID
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
            /*Commit TransAction*/
        End
    Return 1
END
''',
'''
CREATE TABLE [dbo].[Positions](
    [Position_ID] [int] IDENTITY(1,1) NOT NULL,
    [Position_ObjectStatus] [int] NOT NULL,
    [Position_GSMStatus] [int] NOT NULL,
    [Position_GPSStatus] [int] NOT NULL,
    [Position_DateTime] [datetime] NOT NULL,
    [Position_Latitude] [decimal](22, 10) NOT NULL,
    [Position_Longitude] [decimal](22, 10) NOT NULL,
    [Position_Direction] [int] NOT NULL,
    [Position_Speed] [int] NOT NULL,
    [Position_Flash] [int] NOT NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
    CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED
    (
        [Position_ID] ASC
    ) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[Positions]  WITH CHECK ADD  CONSTRAINT [FK_Positions_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[Positions] CHECK CONSTRAINT [FK_Positions_SnapShots]
''',
'''
ALTER TABLE [dbo].[Positions]  WITH CHECK ADD  CONSTRAINT [FK_Positions_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[Positions] CHECK CONSTRAINT [FK_Positions_Sources]
''',
'''
CREATE PROCEDURE [dbo].[Positions_Write]
    @p_SourceCode       nvarchar(250),
    @p_ObjectStatus     int,
    @p_GSMStatus        int,
    @p_GPSStatus        int,
    @p_GPSTime          datetime,
    @p_Latitude         decimal(22,10),
    @p_Longitude        decimal(22,10),
    @p_Direction        int,
    @p_Speed            int,
    @p_Flash            int,
    @p_ShotID           int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @ResultCode         int
    Declare @SourceID           int

    Select @ResultCode = 0
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    if @SourceID is not null
        Begin
            /*Begin TransAction*/

            INSERT INTO
                dbo.Positions
                    (
                        [Position_ObjectStatus],
                        [Position_GSMStatus],
                        [Position_GPSStatus],
                        [Position_DateTime],
                        [Position_Latitude],
                        [Position_Longitude],
                        [Position_Direction],
                        [Position_Speed],
                        [Position_Flash],
                        [Source_ID],
                        [SnapShot_ID]
                    )
                VALUES
                    (
                        @p_ObjectStatus,
                        @p_GSMStatus,
                        @p_GPSStatus,
                        @p_GPSTime,
                        @p_Latitude,
                        @p_Longitude,
                        @p_Direction,
                        @p_Speed,
                        @p_Flash,
                        @SourceID,
                        @p_ShotID
                    )
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
                Select @ResultCode = scope_identity()
            /*Commit TransAction*/
        End
    Return @ResultCode
END
''',
'''
CREATE PROCEDURE [dbo].[Positions_SelectPeriod]
    @p_SourceNumber     int,
    @p_From             datetime,
    @p_To               datetime
AS
BEGIN
    SET NOCOUNT ON;
SELECT
    p.Position_ID,
    p.Position_ObjectStatus,
    p.Position_GSMStatus,
    p.Position_GPSStatus,
    p.Position_DateTime,
    p.Position_Latitude,
    p.Position_Longitude,
    p.Position_Direction,
    p.Position_Speed,
    p.Position_Flash,
    p.Source_ID,
    p.SnapShot_ID,
    s.SnapShot_DateTime
    FROM
        dbo.Positions AS p
            RIGHT OUTER JOIN dbo.SnapShots AS s ON p.SnapShot_ID = s.SnapShot_ID
    WHERE
        (p.Source_ID = dbo.Sources_GetID(@p_SourceNumber))  and
        datediff(ss,@p_From, s.SnapShot_DateTime)>=0        and
        datediff(ss,s.SnapShot_DateTime,@p_To)>=0

END
''',
'''
CREATE TABLE [dbo].[CurrentPositions](
    [CurrentPosition_ID] [int] IDENTITY(1,1) NOT NULL,
    [CurrentPosition_SourceNumber] [int] NOT NULL,
    [CurrentPosition_ObjectStatus] [int] NULL,
    [CurrentPosition_GSMStatus] [int] NULL,
    [CurrentPosition_GPSStatus] [int] NULL,
    [CurrentPosition_DateTime] [datetime] NULL,
    [CurrentPosition_Latitude] [decimal](22, 10) NULL,
    [CurrentPosition_Longitude] [decimal](22, 10) NULL,
    [CurrentPosition_Direction] [int] NULL,
    [CurrentPosition_Speed] [int] NULL,
    [CurrentPosition_Flash] [int] NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
    [Position_ID] [int] NULL,
    CONSTRAINT [PK_CurrentPositions] PRIMARY KEY CLUSTERED
    (
        [CurrentPosition_ID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[CurrentPositions]  WITH CHECK ADD  CONSTRAINT [FK_CurrentPositions_Positions] FOREIGN KEY([Position_ID])
REFERENCES [dbo].[Positions] ([Position_ID])
''',
'''
ALTER TABLE [dbo].[CurrentPositions] CHECK CONSTRAINT [FK_CurrentPositions_Positions]
''',
'''
ALTER TABLE [dbo].[CurrentPositions]  WITH CHECK ADD  CONSTRAINT [FK_CurrentPositions_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[CurrentPositions] CHECK CONSTRAINT [FK_CurrentPositions_SnapShots]
''',
'''
ALTER TABLE [dbo].[CurrentPositions]  WITH CHECK ADD  CONSTRAINT [FK_CurrentPositions_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[CurrentPositions] CHECK CONSTRAINT [FK_CurrentPositions_Sources]
''',
'''
ALTER TABLE [dbo].[CurrentPositions] ADD  CONSTRAINT [DF__CurrentPo__Curre__15502E78]  DEFAULT ((0)) FOR [CurrentPosition_ObjectStatus]
''',
'''
ALTER TABLE [dbo].[CurrentPositions] ADD  CONSTRAINT [DF__CurrentPo__Curre__164452B1]  DEFAULT ((0)) FOR [CurrentPosition_GSMStatus]
''',
'''
ALTER TABLE [dbo].[CurrentPositions] ADD  CONSTRAINT [DF__CurrentPo__Curre__173876EA]  DEFAULT ((0)) FOR [CurrentPosition_GPSStatus]
''',
'''
ALTER TABLE [dbo].[CurrentPositions] ADD  CONSTRAINT [DF__CurrentPo__Curre__182C9B23]  DEFAULT ((0)) FOR [CurrentPosition_Direction]
''',
'''
ALTER TABLE [dbo].[CurrentPositions] ADD  CONSTRAINT [DF__CurrentPo__Curre__1920BF5C]  DEFAULT ((0)) FOR [CurrentPosition_Speed]
''',
'''
ALTER TABLE [dbo].[CurrentPositions] ADD  CONSTRAINT [DF__CurrentPo__Curre__1A14E395]  DEFAULT ((0)) FOR [CurrentPosition_Flash]
''',
'''
CREATE PROCEDURE [dbo].[CurrentPositions_Write]
    @p_SourceCode       nvarchar(250),
    @p_ObjectStatus     int,
    @p_GSMStatus        int,
    @p_GPSStatus        int,
    @p_GPSTime          datetime,
    @p_Latitude         decimal(22,10),
    @p_Longitude        decimal(22,10),
    @p_Direction        int,
    @p_Speed            int,
    @p_Flash            int,
    @p_ShotID           int,
    @p_PositionID       int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @SourceID           int
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    if @SourceID is not null
        Begin
            /*Begin TransAction*/

            Update
                dbo.CurrentPositions
                Set
                    CurrentPosition_ObjectStatus    =   @p_ObjectStatus,
                    CurrentPosition_GSMStatus       =   @p_GSMStatus,
                    CurrentPosition_GPSStatus       =   @p_GPSStatus,
                    CurrentPosition_DateTime        =   @p_GPSTime,
                    CurrentPosition_Latitude        =   @p_Latitude,
                    CurrentPosition_Longitude       =   @p_Longitude,
                    CurrentPosition_Direction       =   @p_Direction,
                    CurrentPosition_Speed           =   @p_Speed,
                    CurrentPosition_Flash           =   @p_Flash,
                    SnapShot_ID                     =   @p_ShotID,
                    Position_ID                     =   @p_PositionID
                Where
                    Source_ID   =   @SourceID
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
            /*Commit TransAction*/
        End
    Return 1
END
''',
'''
CREATE TABLE [dbo].[BaseMeasures](
    [BaseMeasure_ID] [int] IDENTITY(1,1) NOT NULL,
    [BaseMeasure_ObjectStatus] [int] NOT NULL,
    [BaseMeasure_GSMStatus] [int] NOT NULL,
    [BaseMeasure_GPSStatus] [int] NOT NULL,
    [BaseMeasure_CANStatus] [int] NOT NULL,
    [BaseMeasure_PowerStatus] [int] NOT NULL,
    [BaseMeasure_CurrentSignal] [int] NOT NULL,
    [BaseMeasure_PreviousSignal] [int] NOT NULL,
    [BaseMeasure_ChangeSignal] [int] NOT NULL,
    [BaseMeasure_AlarmSignal] [int] NOT NULL,
    [BaseMeasure_Key] [nvarchar](4) NULL,
    [BaseMeasure_DAC1] [int] NOT NULL,
    [BaseMeasure_DAC2] [int] NOT NULL,
    [BaseMeasure_DAC3] [int] NOT NULL,
    [BaseMeasure_DAC4] [int] NOT NULL,
    [BaseMeasure_DACPower] [int] NOT NULL,
    [BaseMeasure_DACBattery] [int] NOT NULL,
    [BaseMeasure_OutputStatus] [int] NOT NULL,
    [BaseMeasure_ApplicationBuffer] [int] NOT NULL,
    [BaseMeasure_Flash] [int] NOT NULL,
    [BaseMeasure_EventTime] [datetime] NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
 CONSTRAINT [PK_BaseMeasures] PRIMARY KEY CLUSTERED
(
    [BaseMeasure_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[BaseMeasures]  WITH CHECK ADD  CONSTRAINT [FK_BaseMeasures_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[BaseMeasures] CHECK CONSTRAINT [FK_BaseMeasures_SnapShots]
''',
'''
ALTER TABLE [dbo].[BaseMeasures]  WITH CHECK ADD  CONSTRAINT [FK_BaseMeasures_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[BaseMeasures] CHECK CONSTRAINT [FK_BaseMeasures_Sources]
''',
'''
ALTER TABLE [dbo].[BaseMeasures] ADD  CONSTRAINT [DF__BaseMeasu__BaseM__09DE7BCC]  DEFAULT (N'') FOR [BaseMeasure_Key]
''',
'''
ALTER TABLE [dbo].[BaseMeasures] ADD  CONSTRAINT [DF__BaseMeasu__BaseM__0AD2A005]  DEFAULT ((0)) FOR [BaseMeasure_DACPower]
''',
'''
ALTER TABLE [dbo].[BaseMeasures] ADD  CONSTRAINT [DF__BaseMeasu__BaseM__0BC6C43E]  DEFAULT ((0)) FOR [BaseMeasure_DACBattery]
''',
'''
ALTER TABLE [dbo].[BaseMeasures] ADD  CONSTRAINT [DF__BaseMeasu__BaseM__0CBAE877]  DEFAULT ((0)) FOR [BaseMeasure_OutputStatus]
''',
'''
ALTER TABLE [dbo].[BaseMeasures] ADD  CONSTRAINT [DF__BaseMeasu__BaseM__0DAF0CB0]  DEFAULT ((0)) FOR [BaseMeasure_ApplicationBuffer]
''',
'''
ALTER TABLE [dbo].[BaseMeasures] ADD  CONSTRAINT [DF__BaseMeasu__BaseM__0EA330E9]  DEFAULT ((0)) FOR [BaseMeasure_Flash]
''',
'''
CREATE PROCEDURE [dbo].[BaseMeasures_Write2]
    @p_SourceCode           nvarchar(250),
    @p_ObjectStatus         int,
    @p_GSMStatus            int,
    @p_GPSStatus            int,
    @p_CANStatus            int,
    @p_PowerStatus          int,
    @p_CurrentSignal        int,
    @p_PreviousSignal       int,
    @p_ChangeSignal         int,
    @p_AlarmSignal          int,
    @p_Key                  nvarchar(4),
    @p_DAC1                 int,
    @p_DAC2                 int,
    @p_DAC3                 int,
    @p_DAC4                 int,
    @p_DACPower             int,
    @p_DACBattery           int,
    @p_OutputStatus         int,
    @p_ApplicationBuffer    int,
    @p_Flash                int,
    @p_EventTime            datetime,
    @p_ShotID               int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @ResultCode         int
    Declare @SourceID           int

    Select @ResultCode = 0
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    if @SourceID is not null
        Begin
            /*Begin TransAction*/

            INSERT INTO
                dbo.BaseMeasures
                    (
                        BaseMeasure_ObjectStatus,
                        BaseMeasure_GSMStatus,
                        BaseMeasure_GPSStatus,
                        BaseMeasure_CANStatus,
                        BaseMeasure_PowerStatus,
                        BaseMeasure_CurrentSignal,
                        BaseMeasure_PreviousSignal,
                        BaseMeasure_ChangeSignal,
                        BaseMeasure_AlarmSignal,
                        BaseMeasure_Key,
                        BaseMeasure_DAC1,
                        BaseMeasure_DAC2,
                        BaseMeasure_DAC3,
                        BaseMeasure_DAC4,
                        BaseMeasure_DACPower,
                        BaseMeasure_DACBattery,
                        BaseMeasure_OutputStatus,
                        BaseMeasure_ApplicationBuffer,
                        BaseMeasure_Flash,
                        BaseMeasure_EventTime,
                        Source_ID,
                        SnapShot_ID
                    )
                VALUES
                    (
                        @p_ObjectStatus     ,
                        @p_GSMStatus        ,
                        @p_GPSStatus        ,
                        @p_CANStatus        ,
                        @p_PowerStatus      ,
                        @p_CurrentSignal    ,
                        @p_PreviousSignal   ,
                        @p_ChangeSignal     ,
                        @p_AlarmSignal      ,
                        @p_Key              ,
                        @p_DAC1             ,
                        @p_DAC2             ,
                        @p_DAC3             ,
                        @p_DAC4             ,
                        @p_DACPower         ,
                        @p_DACBattery       ,
                        @p_OutputStatus     ,
                        @p_ApplicationBuffer,
                        @p_Flash            ,
                        @p_EventTime        ,
                        @SourceID,
                        @p_ShotID
                    )
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
                Select @ResultCode = scope_identity()
            /*Commit TransAction*/
        End
    Return @ResultCode
END
''',
'''
CREATE PROCEDURE [dbo].[Positions_SelectPeriod2]
    @p_SourceID     int,
    @p_BeginPeriod  datetime,
    @p_EndPeriod    datetime
AS
BEGIN
    SET NOCOUNT ON;

    Select
    S.SnapShot_GUID                 as Position_GUID,
    BM.BaseMeasure_EventTime        as Position_EventTime,
    P.Position_DateTime             as Position_DateTime,
    P.Position_GPSStatus            as Position_GPSStatus,
    P.Position_Latitude             as Position_Latitude,
    P.Position_Longitude            as Position_Longitude,
    P.Position_Speed                as Position_Speed,
    P.Position_Direction            as Position_Direction,
    BM.BaseMeasure_CurrentSignal    as Position_Signal,
    SS.Source_SerialNumber          as Position_SourceID
    From
        dbo.SnapShots S
            Inner Join dbo.Positions P on S.SnapShot_ID = P.SnapShot_ID
            Inner Join dbo.BaseMeasures BM on S.SnapShot_ID = BM.SnapShot_ID
            Inner Join dbo.Sources SS on S.Source_ID = SS.Source_ID
    Where
        SS.Source_SerialNumber = @p_SourceID and
        datediff(ss,@p_BeginPeriod,BM.BaseMeasure_EventTime)>=0 and
        datediff(ss,BM.BaseMeasure_EventTime,@p_EndPeriod)>=0
    Order By
        BM.BaseMeasure_EventTime
END
''',
'''
CREATE TABLE [dbo].[CurrentMeasures](
    [CurrentMeasures_ID] [int] IDENTITY(1,1) NOT NULL,
    [CurrentMeasure_SourceNumber] [int] NOT NULL,
    [CurrentMeasure_ObjectStatus] [int] NULL,
    [CurrentMeasure_GSMStatus] [int] NULL,
    [CurrentMeasure_GPSStatus] [int] NULL,
    [CurrentMeasure_CANStatus] [int] NULL,
    [CurrentMeasure_PowerStatus] [int] NULL,
    [CurrentMeasure_CurrentSignal] [int] NULL,
    [CurrentMeasure_PreviousSignal] [int] NULL,
    [CurrentMeasure_ChangeSignal] [int] NULL,
    [CurrentMeasure_AlarmSignal] [int] NULL,
    [CurrentMeasure_Key] [nvarchar](4) NULL,
    [CurrentMeasure_DAC1] [int] NULL,
    [CurrentMeasure_DAC2] [int] NULL,
    [CurrentMeasure_DAC3] [int] NULL,
    [CurrentMeasure_DAC4] [int] NULL,
    [CurrentMeasure_DACPower] [int] NULL,
    [CurrentMeasure_DACBattery] [int] NULL,
    [CurrentMeasure_OutputStatus] [int] NULL,
    [CurrentMeasure_ApplicationBuffer] [int] NULL,
    [CurrentMeasure_Flash] [int] NULL,
    [CurrentMeasure_EventTime] [datetime] NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
    [BaseMeasure_ID] [int] NULL,
 CONSTRAINT [PK_CurrentMeasures] PRIMARY KEY CLUSTERED
(
    [CurrentMeasures_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[CurrentMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentMeasures_BaseMeasures] FOREIGN KEY([BaseMeasure_ID])
REFERENCES [dbo].[BaseMeasures] ([BaseMeasure_ID])
''',
'''
ALTER TABLE [dbo].[CurrentMeasures] CHECK CONSTRAINT [FK_CurrentMeasures_BaseMeasures]
''',
'''
ALTER TABLE [dbo].[CurrentMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentMeasures_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[CurrentMeasures] CHECK CONSTRAINT [FK_CurrentMeasures_SnapShots]
''',
'''
ALTER TABLE [dbo].[CurrentMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentMeasures_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[CurrentMeasures] CHECK CONSTRAINT [FK_CurrentMeasures_Sources]
''',
'''
ALTER TABLE [dbo].[CurrentMeasures] ADD  CONSTRAINT [DF__CurrentMe__Curre__117F9D94]  DEFAULT (N'') FOR [CurrentMeasure_Key]
''',
'''
ALTER TABLE [dbo].[CurrentMeasures] ADD  CONSTRAINT [DF__CurrentMe__Curre__1273C1CD]  DEFAULT ((0)) FOR [CurrentMeasure_DACPower]
''',
'''
ALTER TABLE [dbo].[CurrentMeasures] ADD  CONSTRAINT [DF__CurrentMe__Curre__1367E606]  DEFAULT ((0)) FOR [CurrentMeasure_DACBattery]
''',
'''
CREATE PROCEDURE [dbo].[CurrentMeasures_Write2]
    @p_SourceCode           nvarchar(250),
    @p_ObjectStatus         int,
    @p_GSMStatus            int,
    @p_GPSStatus            int,
    @p_CANStatus            int,
    @p_PowerStatus          int,
    @p_CurrentSignal        int,
    @p_PreviousSignal       int,
    @p_ChangeSignal         int,
    @p_AlarmSignal          int,
    @p_Key                  nvarchar(4),
    @p_DAC1                 int,
    @p_DAC2                 int,
    @p_DAC3                 int,
    @p_DAC4                 int,
    @p_DACPower             int,
    @p_DACBattery           int,
    @p_OutputStatus         int,
    @p_ApplicationBuffer    int,
    @p_Flash                int,
    @p_EventTime            datetime,
    @p_ShotID               int,
    @p_BaseMeasureID        int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @SourceID           int
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    if @SourceID is not null
        Begin
            /*Begin TransAction*/

            Update
                dbo.CurrentMeasures
                Set
                    CurrentMeasure_ObjectStatus         =   @p_ObjectStatus,
                    CurrentMeasure_GSMStatus            =   @p_GSMStatus,
                    CurrentMeasure_GPSStatus            =   @p_GPSStatus,
                    CurrentMeasure_CANStatus            =   @p_CANStatus,
                    CurrentMeasure_PowerStatus          =   @p_PowerStatus,
                    CurrentMeasure_CurrentSignal        =   @p_CurrentSignal,
                    CurrentMeasure_PreviousSignal       =   @p_PreviousSignal,
                    CurrentMeasure_ChangeSignal         =   @p_ChangeSignal,
                    CurrentMeasure_AlarmSignal          =   @p_AlarmSignal,
                    CurrentMeasure_Key                  =   @p_Key,
                    CurrentMeasure_DAC1                 =   @p_DAC1,
                    CurrentMeasure_DAC2                 =   @p_DAC2,
                    CurrentMeasure_DAC3                 =   @p_DAC3,
                    CurrentMeasure_DAC4                 =   @p_DAC4,
                    CurrentMeasure_DACPower             =   @p_DACPower,
                    CurrentMeasure_DACBattery           =   @p_DACBattery,
                    CurrentMeasure_OutputStatus         =   @p_OutputStatus,
                    CurrentMeasure_ApplicationBuffer    =   @p_ApplicationBuffer,
                    CurrentMeasure_Flash                =   @p_Flash,
                    CurrentMeasure_EventTime            =   @p_EventTime,
                    SnapShot_ID                         =   @p_ShotID,
                    BaseMeasure_ID                      =   @p_BaseMeasureID
                Where
                    Source_ID   =   @SourceID
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
            /*Commit TransAction*/
        End
    Return 1
END
''',
'''
CREATE TABLE [dbo].[KdteMeasures](
    [KdteMeasure_ID] [int] IDENTITY(1,1) NOT NULL,
    [KdteMeasure_DateTime] [datetime] NOT NULL,
    [KdteMeasure_Serial] [int] NOT NULL,
    [KdteMeasure_Status] [int] NOT NULL,
    [KdteMeasure_TempStatus] [int] NOT NULL,
    [KdteMeasure_FuelStatus] [int] NOT NULL,
    [KdteMeasure_Temp_1] [int] NOT NULL,
    [KdteMeasure_Temp_2] [int] NOT NULL,
    [KdteMeasure_Temp_3] [int] NOT NULL,
    [KdteMeasure_Temp_4] [int] NOT NULL,
    [KdteMeasure_ADUT_1] [int] NOT NULL,
    [KdteMeasure_ADUT_2] [int] NOT NULL,
    [KdteMeasure_IINC] [int] NOT NULL,
    [KdteMeasure_IDEC] [int] NOT NULL,
    [KdteMeasure_Flash] [int] NOT NULL,
    [Source_ID] [int] NOT NULL,
    [SnapShot_ID] [int] NOT NULL,
 CONSTRAINT [PK_KdteMeasures] PRIMARY KEY CLUSTERED
(
    [KdteMeasure_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[KdteMeasures]  WITH CHECK ADD  CONSTRAINT [FK_KdteMeasures_KdteMeasures] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[KdteMeasures] CHECK CONSTRAINT [FK_KdteMeasures_KdteMeasures]
''',
'''
ALTER TABLE [dbo].[KdteMeasures]  WITH CHECK ADD  CONSTRAINT [FK_KdteMeasures_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[KdteMeasures] CHECK CONSTRAINT [FK_KdteMeasures_Sources]
''',
'''
CREATE PROCEDURE [dbo].[KDTEMeasures_Write]
    @p_ShotID       int,
    @p_SourceCode   nvarchar(250),
    @p_KDTESerial   int,
    @p_EventTime    datetime,
    @p_Status       int,
    @p_TempStatus   int,
    @p_FuelStatus   int,
    @p_Temp_1       int,
    @p_Temp_2       int,
    @p_Temp_3       int,
    @p_Temp_4       int,
    @p_ADUT_1       int,
    @p_ADUT_2       int,
    @p_IInc         int,
    @p_IDec         int,
    @p_Flash        int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @ResultCode         int
    Declare @SourceID           int

    Select @ResultCode = 0
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1

    if @SourceID is not null
        Begin
            /*Begin TransAction*/

                INSERT INTO
                    dbo.KdteMeasures
                        (
                            KdteMeasure_DateTime,
                            KdteMeasure_Serial,
                            KdteMeasure_Status,
                            KdteMeasure_TempStatus,
                            KdteMeasure_FuelStatus,
                            KdteMeasure_Temp_1,
                            KdteMeasure_Temp_2,
                            KdteMeasure_Temp_3,
                            KdteMeasure_Temp_4,
                            KdteMeasure_ADUT_1,
                            KdteMeasure_ADUT_2,
                            KdteMeasure_IINC,
                            KdteMeasure_IDEC,
                            KdteMeasure_Flash,
                            Source_ID,
                            SnapShot_ID
                        )
                    VALUES
                        (
                            @p_EventTime,
                            @p_KDTESerial,
                            @p_Status,
                            @p_TempStatus,
                            @p_FuelStatus,
                            @p_Temp_1,
                            @p_Temp_2,
                            @p_Temp_3,
                            @p_Temp_4,
                            @p_ADUT_1,
                            @p_ADUT_2,
                            @p_IInc,
                            @p_IDec,
                            @p_Flash,
                            @SourceID,
                            @p_ShotID
                        )
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
                Select @ResultCode = scope_identity()
            /*Commit TransAction*/
        End
    Return @ResultCode
END
''',
'''
CREATE TABLE [dbo].[CurrentKdteMeasures](
    [CurrentKdteMeasure_ID] [int] IDENTITY(1,1) NOT NULL,
    [CurrentKdteMeasure_DateTime] [datetime] NULL,
    [CurrentKdteMeasure_Serial] [int] NOT NULL,
    [CurrentKdteMeasure_Status] [int] NULL,
    [CurrentKdteMeasure_TempStatus] [int] NULL,
    [CurrentKdteMeasure_FuelStatus] [int] NULL,
    [CurrentKdteMeasure_Temp_1] [int] NULL,
    [CurrentKdteMeasure_Temp_2] [int] NULL,
    [CurrentKdteMeasure_Temp_3] [int] NULL,
    [CurrentKdteMeasure_Temp_4] [int] NULL,
    [CurrentKdteMeasure_ADUT_1] [int] NULL,
    [CurrentKdteMeasure_ADUT_2] [int] NULL,
    [CurrentKdteMeasure_IINC] [int] NULL,
    [CurrentKdteMeasure_IDEC] [int] NULL,
    [CurrentKdteMeasure_Flash] [int] NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
    [KdteMeasure_ID] [int] NULL,
 CONSTRAINT [PK_CurrentKdteMeasure] PRIMARY KEY CLUSTERED
(
    [CurrentKdteMeasure_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[CurrentKdteMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentKdteMeasures_KdteMeasures] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[CurrentKdteMeasures] CHECK CONSTRAINT [FK_CurrentKdteMeasures_KdteMeasures]
''',
'''
ALTER TABLE [dbo].[CurrentKdteMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentKdteMeasures_KdteMeasures1] FOREIGN KEY([KdteMeasure_ID])
REFERENCES [dbo].[KdteMeasures] ([KdteMeasure_ID])
''',
'''
ALTER TABLE [dbo].[CurrentKdteMeasures] CHECK CONSTRAINT [FK_CurrentKdteMeasures_KdteMeasures1]
''',
'''
ALTER TABLE [dbo].[CurrentKdteMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentKdteMeasures_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[CurrentKdteMeasures] CHECK CONSTRAINT [FK_CurrentKdteMeasures_Sources]
''',
'''
CREATE PROCEDURE [dbo].[CurrentKDTEMeasures_Write]
    @p_ShotID           int,
    @p_SourceCode       nvarchar(250),
    @p_KDTESerial       int,
    @p_EventTime        datetime,
    @p_Status           int,
    @p_TempStatus       int,
    @p_FuelStatus       int,
    @p_Temp_1           int,
    @p_Temp_2           int,
    @p_Temp_3           int,
    @p_Temp_4           int,
    @p_ADUT_1           int,
    @p_ADUT_2           int,
    @p_IInc             int,
    @p_IDec             int,
    @p_Flash            int,
    @p_KDTEMeasureID    int
AS
BEGIN
    SET NOCOUNT ON;

    Declare @SourceID           int

    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    if @SourceID is not null
        Begin
            /*Begin TransAction*/
                Update
                    dbo.CurrentKDTEMeasures
                    Set
                        [CurrentKdteMeasure_DateTime]   = @p_EventTime,
                        [CurrentKdteMeasure_Status]     = @p_Status,
                        [CurrentKdteMeasure_TempStatus] = @p_TempStatus,
                        [CurrentKdteMeasure_FuelStatus] = @p_FuelStatus,
                        [CurrentKdteMeasure_Temp_1]     = @p_Temp_1,
                        [CurrentKdteMeasure_Temp_2]     = @p_Temp_2,
                        [CurrentKdteMeasure_Temp_3]     = @p_Temp_3,
                        [CurrentKdteMeasure_Temp_4]     = @p_Temp_4,
                        [CurrentKdteMeasure_ADUT_1]     = @p_ADUT_1,
                        [CurrentKdteMeasure_ADUT_2]     = @p_ADUT_2,
                        [CurrentKdteMeasure_IINC]       = @p_IInc,
                        [CurrentKdteMeasure_IDEC]       = @p_IDec,
                        [CurrentKdteMeasure_Flash]      = @p_Flash,
                        [Source_ID]                     = @SourceID,
                        [SnapShot_ID]                   = @p_ShotID,
                        [KdteMeasure_ID]                = @p_KDTEMeasureID
                    Where
                        CurrentKDTEMeasure_Serial = @p_KDTESerial
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
            /*Commit TransAction*/
        End
    Return 1
END
''',
'''
CREATE TABLE [dbo].[KDutMeasures](
    [KDutMeasure_ID] [int] IDENTITY(1,1) NOT NULL,
    [KDutMeasure_DateTime] [datetime] NOT NULL,
    [KDutMeasure_Serial] [int] NOT NULL,
    [KDutMeasure_LLS1] [int] NOT NULL,
    [KDutMeasure_LLS2] [int] NOT NULL,
    [KDutMeasure_ADUT1] [int] NOT NULL,
    [KDutMeasure_ADUT2] [int] NOT NULL,
    [KDutMeasure_IINC] [int] NOT NULL,
    [KDutMeasure_IDEC] [int] NOT NULL,
    [KDutMeasure_Flash] [int] NOT NULL,
    [KDutMeasure_Status] [int] NOT NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
 CONSTRAINT [PK_KDutMeasures] PRIMARY KEY CLUSTERED
(
    [KDutMeasure_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[KDutMeasures]  WITH CHECK ADD  CONSTRAINT [FK_KDutMeasures_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[KDutMeasures] CHECK CONSTRAINT [FK_KDutMeasures_SnapShots]
''',
'''
ALTER TABLE [dbo].[KDutMeasures]  WITH CHECK ADD  CONSTRAINT [FK_KDutMeasures_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[KDutMeasures] CHECK CONSTRAINT [FK_KDutMeasures_Sources]
''',
'''
CREATE PROCEDURE [dbo].[KDUTMeasures_Write]
    @p_ShotID       int,
    @p_SourceCode   nvarchar(250),
    @p_KDutSerial   int,
    @p_EventTime    datetime,
    @p_KDutStatus   int,
    @p_LLS1         int,
    @p_LLS2         int,
    @p_ADUT1        int,
    @p_ADUT2        int,
    @p_IInc         int,
    @p_IDec         int,
    @p_Flash        int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @ResultCode         int
    Declare @SourceID           int

    Select @ResultCode = 0
    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1

    if @SourceID is not null
        Begin
            /*Begin TransAction*/

                INSERT INTO
                    dbo.KDutMeasures
                        (
                            [KDutMeasure_DateTime],
                            [KDutMeasure_Serial],
                            [KDutMeasure_LLS1],
                            [KDutMeasure_LLS2],
                            [KDutMeasure_ADUT1],
                            [KDutMeasure_ADUT2],
                            [KDutMeasure_IINC],
                            [KDutMeasure_IDEC],
                            [KDutMeasure_Flash],
                            [KDutMeasure_Status],
                            [Source_ID],
                            [SnapShot_ID]
                        )
                    VALUES
                        (
                            @p_EventTime,
                            @p_KDutSerial,
                            @p_LLS1,
                            @p_LLS2,
                            @p_ADUT1,
                            @p_ADUT2,
                            @p_IInc,
                            @p_IDec,
                            @p_Flash,
                            @p_KDutStatus,
                            @SourceID,
                            @p_ShotID
                        )
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
                Select @ResultCode = scope_identity()
            /*Commit TransAction*/
        End
    Return @ResultCode
END
''',
'''
CREATE TABLE [dbo].[CurrentKDUTMeasures](
    [CurrentKDUTMeasure_ID] [int] IDENTITY(1,1) NOT NULL,
    [CurrentKDUTMeasure_Serial] [int] NOT NULL,
    [CurrentKDUTMeasure_DateTime] [datetime] NULL,
    [CurrentKDUTMeasure_LLS1] [int] NULL,
    [CurrentKDUTMeasure_LLS2] [int] NULL,
    [CurrentKDUTMeasure_ADUT1] [int] NULL,
    [CurrentKDUTMeasure_ADUT2] [int] NULL,
    [CurrentKDUTMeasure_IINC] [int] NULL,
    [CurrentKDUTMeasure_IDEC] [int] NULL,
    [CurrentKDUTMeasure_Flash] [int] NULL,
    [CurrentKDUTMeasure_Status] [int] NULL,
    [Source_ID] [int] NULL,
    [SnapShot_ID] [int] NULL,
    [KDUTMeasure_ID] [int] NULL,
 CONSTRAINT [PK_CurrentKDUTMeasures] PRIMARY KEY CLUSTERED
(
    [CurrentKDUTMeasure_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
''',
'''
ALTER TABLE [dbo].[CurrentKDUTMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentKDUTMeasures_KDutMeasures] FOREIGN KEY([KDUTMeasure_ID])
REFERENCES [dbo].[KDutMeasures] ([KDutMeasure_ID])
''',
'''
ALTER TABLE [dbo].[CurrentKDUTMeasures] CHECK CONSTRAINT [FK_CurrentKDUTMeasures_KDutMeasures]
''',
'''
ALTER TABLE [dbo].[CurrentKDUTMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentKDUTMeasures_SnapShots] FOREIGN KEY([SnapShot_ID])
REFERENCES [dbo].[SnapShots] ([SnapShot_ID])
''',
'''
ALTER TABLE [dbo].[CurrentKDUTMeasures] CHECK CONSTRAINT [FK_CurrentKDUTMeasures_SnapShots]
''',
'''
ALTER TABLE [dbo].[CurrentKDUTMeasures]  WITH CHECK ADD  CONSTRAINT [FK_CurrentKDUTMeasures_Sources] FOREIGN KEY([Source_ID])
REFERENCES [dbo].[Sources] ([Source_ID])
''',
'''
ALTER TABLE [dbo].[CurrentKDUTMeasures] CHECK CONSTRAINT [FK_CurrentKDUTMeasures_Sources]
''',
'''
CREATE PROCEDURE [dbo].[CurrentKDUTMeasures_Write]
    @p_ShotID           int,
    @p_SourceCode       nvarchar(250),
    @p_KDutSerial       int,
    @p_EventTime        datetime,
    @p_KDutStatus       int,
    @p_LLS1             int,
    @p_LLS2             int,
    @p_ADUT1            int,
    @p_ADUT2            int,
    @p_IInc             int,
    @p_IDec             int,
    @p_Flash            int,
    @p_KDUTMeasureID    int
AS
BEGIN
    SET NOCOUNT ON;

    Declare @SourceID           int

    Select @SourceID = Source_ID From dbo.Sources Where Source_Code  = upper(@p_SourceCode) and Source_Activity = 1
    if @SourceID is not null
        Begin
            /*Begin TransAction*/

                Update
                    dbo.CurrentKDutMeasures
                    Set
                        [CurrentKDutMeasure_DateTime]   =   @p_EventTime,
                        [CurrentKDutMeasure_LLS1]       =   @p_LLS1,
                        [CurrentKDutMeasure_LLS2]       =   @p_LLS2,
                        [CurrentKDutMeasure_ADUT1]      =   @p_ADUT1,
                        [CurrentKDutMeasure_ADUT2]      =   @p_ADUT2,
                        [CurrentKDutMeasure_IINC]       =   @p_IInc,
                        [CurrentKDutMeasure_IDEC]       =   @p_IDec,
                        [CurrentKDutMeasure_Flash]      =   @p_Flash,
                        [CurrentKDutMeasure_Status]     =   @p_KDutStatus,
                        [Source_ID]                     =   @SourceID,
                        [SnapShot_ID]                   =   @p_ShotID,
                        [KDutMeasure_ID]                =   @p_KDUTMeasureID
                    Where
                        CurrentKDutMeasure_Serial = @p_KDutSerial
                if @@error<>0
                    Begin
                        /*RollBack TransAction*/
                        Return 0
                    End
            /*Commit TransAction*/
        End
    Return 1
END
''',
'''
CREATE PROCEDURE [dbo].[Sources_EngineRegistration]
    @p_SourceNumber     int
AS
BEGIN
    SET NOCOUNT ON;
    Declare @SourceID   int

    Select @SourceID = Source_ID From dbo.Sources Where Source_SerialNumber = @p_SourceNumber

    if not exists ( Select * From dbo.CurrentSnapShots Where Source_ID = @SourceID )
        Begin
            Insert InTo dbo.CurrentSnapShots ( Source_ID ) Values ( @SourceID )
        End

    if not exists ( Select * From dbo.CurrentPositions Where Source_ID = @SourceID )
        Begin
            Insert InTo dbo.CurrentPositions ( CurrentPosition_SourceNumber, Source_ID ) Values ( @p_SourceNumber, @SourceID )
        End

    if not exists ( Select * From dbo.CurrentMeasures Where Source_ID = @SourceID )
        Begin
            Insert InTo dbo.CurrentMeasures ( CurrentMeasure_SourceNumber, Source_ID ) Values ( @p_SourceNumber, @SourceID )
        End

    Update dbo.Sources Set Source_Activity=1 Where Source_ID = @SourceID
END
''',
'''
CREATE PROCEDURE [dbo].[Sources_Disable]
    @p_SourceNumber     int
AS
BEGIN
    SET NOCOUNT ON;
    Update dbo.Sources Set Source_Activity=0 Where Source_SerialNumber = @p_SourceNumber
END
'''
]
    c = dbcon.cursor()
    for e in create_exprs:
        c.execute(e)
    dbcon.commit()
    c.close()

def importData(dbcon, f):
    imported_n = 0
    return imported_n

def purgeData(dbcon, equip_ids, time_from, time_to):
    return 0

def exportData(dbcon, f, equip_ids, time_from, time_to):
    exported_n = 0
    return exported_n

class MSSQLStorageClientService(FrontendClientService):
    logged_n = 0L
    inserted_n = 0L
    processingLogDb = False
    process_n = 0L
    def __init__(self, h, p, dbpool, log_file = None):
        FrontendClientService.__init__(self, h, p)
        self.dbpool = dbpool
        self.log_file = log_file
        if None != self.log_file:
            if not os.access(self.log_file, os.F_OK):
                self.createLogDb()
            else:
                self.importLogDb()
    def importLogDb(self):
        tmp_con = sqlite3.connect(self.log_file)
        c = tmp_con.cursor()
        c.execute('SELECT COUNT(*) FROM LOG')
        r = c.fetchone();
        if None != r:
            self.logged_n = int(r[0])
        tmp_con.close()
    def createLogDb(self):
        tmp_con = sqlite3.connect(self.log_file)
        c = tmp_con.cursor()
        c.execute('CREATE TABLE LOG(ID INTEGER PRIMARY KEY, DATA TEXT)')
        tmp_con.close()
    def logInsertSuccess(self, result, log_id):
        self.process_n = self.process_n - 1
        if 0 == self.process_n:
            self.processingLogDb = False
        self.logged_n = self.logged_n - 1
        self.inserted_n = self.inserted_n + 1
        tmp_con = sqlite3.connect(self.log_file)
        c = tmp_con.cursor()
        c.execute('DELETE FROM LOG WHERE ID=?',(log_id,))
        tmp_con.commit()
        tmp_con.close()
    def logInsertFailure(self, failure):
        self.process_n = self.process_n - 1
        if 0 == self.process_n:
            self.processingLogDb = False
        log.msg("StorageClientService:logged record failed to insert : %s" % (failure.getTraceback()))
    def processLogDb(self):
        if None != self.log_file:
            if not self.processingLogDb:
                self.processingLogDb = True
                tmp_con = sqlite3.connect(self.log_file)
                c = tmp_con.cursor()
                c.execute('SELECT ID, DATA FROM LOG ORDER BY ID DESC')
                r = c.fetchone()
                self.process_n = 0
                while None != r:
                    if type(r[1]) == types.UnicodeType:
                        p = r[1].encode('ascii')
                    else:
                        p = r[1]
                    s = cPickle.loads(p)
                    log_id = r[0]
                    self.insert(s, lambda x : self.logInsertSuccess(x, log_id), self.logInsertFailure, False)
                    self.process_n = self.process_n + 1
                    r = c.fetchone()
                tmp_con.close()
                return True
            else:
                return False
        return False
    def insertSuccess(self, result):
#        log.msg("StorageClientService:record inserted, result : %r" % (result))
        self.inserted_n = self.inserted_n + 1
    def insertFailure(self, failure, s):
        log.msg("StorageClientService:record failed to insert : %s" % (failure.getTraceback()))
        if None != failure.check(self.dbpool.dbapi.OperationalError, self.dbpool.dbapi.InternalError):
            if None != self.log_file:
                tmp_con = sqlite3.connect(self.log_file)
                c = tmp_con.cursor()
                c.execute("INSERT INTO LOG(DATA) VALUES(?)",(cPickle.dumps(s),))
                tmp_con.commit()
                tmp_con.close()
                self.logged_n = self.logged_n + 1
    def insert(self, s, callback_handler, errback_handler, from_log = False):
        self.dbpool.runInteraction(self.insertInteraction, s, from_log).addCallback(callback_handler).addErrback(errback_handler)
    def insertInteraction(self, cur, s, from_log):
        lg_fields, lg_dict = extractLineguardFields(s)
        if None != lg_fields:
            #lg_fields = (equip_id, request_str, unix_state_date_time, unix_created_date_time, unix_loc_date_time, status, gps_status, alarms_old, alarms, alarms_now, alarms_was, outs, who, latitude, longitude, course, speed, adcs[1], adcs[2], adcs[3], adcs[4], f_addr, power, battery, gsm_status, can_status, power_status)
            if s.has_key('StateTime') and None != s['StateTime']:
                state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
                created_time = datetime.datetime.utcfromtimestamp(s['CreatedTime'])
                cur.execute('DECLARE @snapshot_id INT;EXECUTE @snapshot_id=[dbo].[Snapshots_Write] @p_ShotGUID=%s,@p_GateCode=%s,@p_SourceCode=%s, @p_DestinationCode=%s,@p_ShotTime=%s,@p_ReceiveTime=%s,@p_Direction=%s,@p_Channel=%s,@p_Entity=%s,@p_SubEntity=%s,@p_ReTranslation=%s;SELECT @snapshot_id',
                (s['GUID'],"GATE.LINEGUARD",'%d' % (s['EquipId'],),"",state_time,created_time,"RECEIVE","GPRS","DATA","INFO",0))
                r = cur.fetchone()
                if r != None:
                    snapshot_id = r[0]
                    if 0 == snapshot_id:
                        raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[SnapShots_Write]')
                else:
                    raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[Snapshots_Write]')
                if not from_log:
                    cur.execute('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentSnapshots_Write] @p_ShotGUID=%s,@p_GateCode=%s,@p_SourceCode=%s,@p_DestinationCode=%s,@p_ShotTime=%s,@p_ReceiveTime=%s,@p_Direction=%s,@p_Channel=%s,@p_Entity=%s,@p_SubEntity=%s,@p_ReTranslation=%s,@p_ShotID=%s;SELECT @error_code',
                (s['GUID'],"GATE.LINEGUARD",'%d' % (s['EquipId'],),"",state_time,created_time,"RECEIVE","GPRS","DATA","INFO",0, snapshot_id))
                    r = cur.fetchone()
                    if r != None:
                        error_code = r[0]
                        if 0 == error_code:
                            print s
                            raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[CurrentSnapshots_Write]')
                    else:
                        raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[CurrentSnapshots_Write]')
                if s.has_key('Location') and s.has_key('Index') and None != s['Index']:
                    location_time = datetime.datetime.utcfromtimestamp(s['Location']['LocationTime'])
                    cur.execute('DECLARE @position_id INT;EXECUTE @position_id=[dbo].[Positions_Write] @p_SourceCode=%s,@p_ObjectStatus=%s,@p_GSMStatus=%s,@p_GPSStatus=%s,@p_GPSTime=%s,@p_Latitude=%s,@p_Longitude=%s,@p_Direction=%s,@p_Speed=%s,@p_Flash=%s,@p_ShotID=%s;SELECT @position_id',
                     ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],location_time,s['Location']['Latitude'],s['Location']['Longitude'],int(s['Location']['Course']/2.),int(s['Location']['Velocity']/1.85), s['Index'], snapshot_id))
                    r = cur.fetchone()
                    if r != None:
                        position_id = r[0]
                        if 0 == position_id:
                            raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[Positions_Write]')
                    else:
                        raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[Positions_Write]')

                    if not from_log:
                        if (lg_dict['status'] & 0x08) == 0:
                            cur.execute('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentPositions_Write] @p_SourceCode=%s,@p_ObjectStatus=%s,@p_GSMStatus=%s,@p_GPSStatus=%s,@p_GPSTime=%s,@p_Latitude=%s,@p_Longitude=%s,@p_Direction=%s,@p_Speed=%s,@p_Flash=%s,@p_ShotID=%s,@p_PositionID=%s;SELECT @error_code',
                             ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],location_time,s['Location']['Latitude'],s['Location']['Longitude'],int(s['Location']['Course']/2.),int(s['Location']['Velocity']/1.85), s['Index'], snapshot_id, position_id))
                            r = cur.fetchone()
                            if r != None:
                                error_code = r[0]
                                if 0 == error_code:
                                    raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[CurrentPositions_Write]')
                            else:
                                raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[CurrentPositions_Write]')
                if s.has_key('Index') and None != s['Index']:
                    cur.execute('DECLARE @basemeasure_id INT;EXECUTE @basemeasure_id=[dbo].[BaseMeasures_Write2] %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d;SELECT @basemeasure_id;',
                     ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],lg_dict['can_status'],lg_dict['power_status'],lg_dict['alarms_now'],lg_dict['alarms_old'],lg_dict['alarms'],lg_dict['alarms_was'],'%d' % (lg_dict['who'],),int(0.5 + lg_dict['adc1']*(1024 / 2.56)),int(0.5 + lg_dict['adc2']*(1024 / 2.56)),int(0.5 + lg_dict['adc3']*(1024 / 2.56)),int(0.5 + lg_dict['adc4']*(1024 / 2.56)),int(0.5 + lg_dict['power']*(1024 / 2.56)),int(0.5 + lg_dict['battery']*(1024 / 2.56)),lg_dict['outs'],0,s['Index'],state_time,snapshot_id))
                    r = cur.fetchone()
                    if r != None:
                        basemeasure_id = r[0]
                        if 0 == basemeasure_id:
                            raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[BaseMeasures_Write2]')
                    else:
                        raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[BaseMeasures_Write2]')
                    if not from_log:
                        if (lg_dict['status'] & 0x08) == 0:
                            cur.execute('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentMeasures_Write2] %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s;SELECT @error_code;',
                             ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],lg_dict['can_status'],lg_dict['power_status'],lg_dict['alarms_now'],lg_dict['alarms_old'],lg_dict['alarms'],lg_dict['alarms_was'],'%d' % (lg_dict['who'],),int(0.5 + lg_dict['adc1']*(1024 / 2.56)),int(0.5 + lg_dict['adc2']*(1024 / 2.56)),int(0.5 + lg_dict['adc3']*(1024 / 2.56)),int(0.5 + lg_dict['adc4']*(1024 / 2.56)),int(0.5 + lg_dict['power']*(1024 / 2.56)),int(0.5 + lg_dict['battery']*(1024 / 2.56)),lg_dict['outs'],0,s['Index'],state_time,snapshot_id,basemeasure_id))
                            r = cur.fetchone()
                            if r != None:
                                error_code = r[0]
                                if 0 == error_code:
                                    raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[CurrentMeasures_Write2]')
                            else:
                                raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[CurrentMeasures_Write2]')
                if s.has_key('FuelLevelControllerId'):
                    if s.has_key('FuelSensor') and s['FuelSensor'].has_key('Index') and s.has_key('FuelCounter') and s.has_key('State') and s['State'].has_key('Adcs'):
                        controller_status = 0
                        controller_id = s['FuelLevelControllerId']
                        level_1 = s['FuelSensor']['Level']['1']
                        level_2 = s['FuelSensor']['Level']['2']
                        adc_1 = int(0.5 + s['State']['Adcs']['4']*4096/5.0)
                        adc_2 = int(0.5 + s['State']['Adcs']['5']*4096/5.0)
                        count_1 = s['FuelCounter']['Count']['1']
                        count_2 = s['FuelCounter']['Count']['2']
                        idx = s['FuelSensor']['Index']
                        if s.has_key('Flags'):
                            if s['Flags'].has_key('FuelLevelControllerOnlineData'):
                                if not s['Flags']['FuelLevelControllerOnlineData']:
                                    controller_status = controller_status | 0x08
                            if s['Flags'].has_key('FuelLevelControllerIsRequest'):
                                if s['Flags']['FuelLevelControllerIsRequest']:
                                    controller_status = controller_status | 0x80
                            if s['Flags'].has_key('FuelLevelControllerConfigMemoryError'):
                                controller_status = controller_status | 0x01
                            if s['Flags'].has_key('FuelLevelControllerHardwareBusError'):
                                controller_status = controller_status | 0x02
                            if s['Flags'].has_key('FuelLevelControllerCounterError'):
                                controller_status = controller_status | 0x04
                            if s['Flags'].has_key('FuelLevelControllerChannel1Error'):
                                controller_status = controller_status | 0x10
                            if s['Flags'].has_key('FuelLevelControllerChannel2Error'):
                                controller_status = controller_status | 0x20
                            if s['Flags'].has_key('FuelLevelControllerPowerOnEvent'):
                                controller_status = controller_status | 0x40
                        cur.execute('DECLARE @kdutmeasure_id INT;EXECUTE @kdutmeasure_id=[dbo].[KDUTMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDutSerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_LLS1=%s,@p_LLS2=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s;SELECT @kdutmeasure_id',
                         (snapshot_id,'%d'%(s['EquipId'],),controller_id,state_time,controller_status,level_1,level_2,adc_1,adc_2,count_1,count_2,idx))
                        r = cur.fetchone()
                        if r != None:
                            kdutmeasure_id = r[0]
                            if 0 == kdutmeasure_id:
                                raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[KDUTMeasures_Write]')
                        else:
                            raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[KDUTMeasures_Write]')
                        if (controller_status & 0x08) == 0:
                            cur.execute('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentKDUTMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDutSerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_LLS1=%s,@p_LLS2=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s,@p_KDUTMeasureID=%s;SELECT @error_code',
                             (snapshot_id,'%d'%(s['EquipId'],),controller_id,state_time,controller_status,level_1,level_2,adc_1,adc_2,count_1,count_2,idx,kdutmeasure_id))
                            r = cur.fetchone()
                            if r != None:
                                error_code = r[0]
                                if 0 == error_code:
                                    raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[CurrentKDUTMeasures_Write]')
                            else:
                                raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[CurrentKDUTMeasures_Write]')
                    else:
                        log.msg('StorageClientService: have not found FuelSensora or FuelSensor.Index or FuelCounter or State.Adcs fields in "%s"' % (s,))
                else:
                    if s.has_key('FuelSensor'):
                        controller_status = 0
                        controller_id = s['EquipId'] & 0xffff
                        level_1 = s['FuelSensor']['Level']['1']
                        if s['FuelSensor']['Level'].has_key('2'):
                            level_2 = s['FuelSensor']['Level']['2']
                        else:
                            level_2 = 0
                        adc_1 = 0
                        adc_2 = 0
                        count_1 = 0
                        count_2 = 0
                        idx = 0
                        if s.has_key('Flags'):
                            if s['Flags'].has_key('Online'):
                                if not s['Flags']['Online']:
                                    controller_status = controller_status | 0x08
                            if s["Flags"].has_key("IsRequest"):
                                if s["Flags"]["IsRequest"]:
                                    controller_status = controller_status | 0x80
                        cur.execute('DECLARE @kdutmeasure_id INT;EXECUTE @kdutmeasure_id=[dbo].[KDUTMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDutSerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_LLS1=%s,@p_LLS2=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s;SELECT @kdutmeasure_id',
                         (snapshot_id,'%d'%(s['EquipId'],),controller_id,state_time,controller_status,level_1,level_2,adc_1,adc_2,count_1,count_2,idx))
                        r = cur.fetchone()
                        if r != None:
                            kdutmeasure_id = r[0]
                            if 0 == kdutmeasure_id:
                                raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[KDUTMeasures_Write]')
                        else:
                            raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[KDUTMeasures_Write]')

                        if (controller_status & 0x08) == 0:
                            cur.execute('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentKDUTMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDutSerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_LLS1=%s,@p_LLS2=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s,@p_KDUTMeasureID=%s;SELECT @error_code',
                             (snapshot_id,'%d'%(s['EquipId'],),controller_id,state_time,controller_status,level_1,level_2,adc_1,adc_2,count_1,count_2,idx,kdutmeasure_id))
                            r = cur.fetchone()
                            if r != None:
                                error_code = r[0]
                                if 0 == error_code:
                                    raise self.dbpool.dbapi.OperationalError('Error result executing [dbo].[CurrentKDUTMeasures_Write]')
                            else:
                                raise self.dbpool.dbapi.OperationalError('Error getting result of executing [dbo].[CurrentKDUTMeasures_Write]')
            #else:
                #log.msg('StorageClientService: have not found StateTime field in "%s"' % (s,))
        else:
            log.msg('StorageClientService:error extracting lggw protocol fields from "%s"' % (j))

    def stateReport(self, serial, ts, j):
        s = json.loads(j)
        self.insert(s, self.insertSuccess, lambda x: self.insertFailure(x, s))
        #self.insertSnapshot(s, lambda x: self.insertSnapshotSuccess(x, s), lambda x: self.insertSnapshotFailure(x, s))

    def insertSnapshot(self, s, callback, errback):
        if s.has_key('StateTime') and None != s['StateTime']:
            state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
            created_time = datetime.datetime.utcfromtimestamp(s['CreatedTime'])
            self.dbpool.runQuery('DECLARE @snapshot_id INT;EXECUTE @snapshot_id=[dbo].[Snapshots_Write] @p_ShotGUID=%s,@p_GateCode=%s,@p_SourceCode=%s, @p_DestinationCode=%s,@p_ShotTime=%s,@p_ReceiveTime=%s,@p_Direction=%s,@p_Channel=%s,@p_Entity=%s,@p_SubEntity=%s,@p_ReTranslation=%s;SELECT @snapshot_id',
            (s['GUID'],"GATE.LINEGUARD",'%d' % (s['EquipId'],),"",state_time,created_time,"RECEIVE","GPRS","DATA","INFO",0)).addCallback(callback).addErrback(errback)
        #else:
            #log.msg('StorageClientService: have not found StateTime field in "%s"' % (s,))

    def insertSnapshotSuccess(self, result, s):
#        log.msg("StorageClientService:snapshot record inserted, result : %r" % (result))
        if result != None:
            snapshot_id = result[0][0]
            if 0 == snapshot_id:
                log.msg('StorageClientService:error result executing [dbo].[SnapShots_Write]')
            else:
                self.inserted_n = self.inserted_n + 1
                self.insertCurrentSnapshot(s, snapshot_id, lambda x: self.insertCurrentSnapshotSuccess(x, s, snapshot_id), lambda x: self.insertCurrentSnapshotFailure(x, s, snapshot_id))
                lg_fields, lg_dict = extractLineguardFields(s)
                if None != lg_fields:
                    self.insertPosition(s, snapshot_id, lg_dict, lambda x: self.insertPositionSuccess(x, s, snapshot_id, lg_dict), lambda x: self.insertPositionFailure(x, s, snapshot_id, lg_dict))
                    self.insertBaseMeasures(s, snapshot_id, lg_dict, lambda x: self.insertBaseMeasuresSuccess(x, s, snapshot_id, lg_dict), lambda x: self.insertBaseMeasuresFailure(x, s, snapshot_id, lg_dict))
                    if s.has_key('FuelLevelControllerId'):
                        if s.has_key('FuelSensor') and s['FuelSensor'].has_key('Index') and s.has_key('FuelCounter') and s.has_key('State') and s['State'].has_key('Adcs'):
                            controller_status = 0
                            controller_id = s['FuelLevelControllerId']
                            level_1 = s['FuelSensor']['Level']['1']
                            level_2 = s['FuelSensor']['Level']['2']
                            adc_1 = int(0.5 + s['State']['Adcs']['4']*4096/5.0)
                            adc_2 = int(0.5 + s['State']['Adcs']['5']*4096/5.0)
                            count_1 = s['FuelCounter']['Count']['1']
                            count_2 = s['FuelCounter']['Count']['2']
                            idx = s['FuelSensor']['Index']
                            if s.has_key('Flags'):
                                if s['Flags'].has_key('FuelLevelControllerOnlineData'):
                                    if not s['Flags']['FuelLevelControllerOnlineData']:
                                        controller_status = controller_status | 0x08
                                if s['Flags'].has_key('FuelLevelControllerIsRequest'):
                                    if s['Flags']['FuelLevelControllerIsRequest']:
                                        controller_status = controller_status | 0x80
                                if s['Flags'].has_key('FuelLevelControllerConfigMemoryError'):
                                    controller_status = controller_status | 0x01
                                if s['Flags'].has_key('FuelLevelControllerHardwareBusError'):
                                    controller_status = controller_status | 0x02
                                if s['Flags'].has_key('FuelLevelControllerCounterError'):
                                    controller_status = controller_status | 0x04
                                if s['Flags'].has_key('FuelLevelControllerChannel1Error'):
                                    controller_status = controller_status | 0x10
                                if s['Flags'].has_key('FuelLevelControllerChannel2Error'):
                                    controller_status = controller_status | 0x20
                                if s['Flags'].has_key('FuelLevelControllerPowerOnEvent'):
                                    controller_status = controller_status | 0x40
                            state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
                            serial = s['EquipId']
                            self.insertKDUTMeasures(serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, lambda x: self.insertKDUTMeasuresSuccess(x, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx), lambda x: self.insertKDUTMeasuresFaulure(x, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx))
                        else:
                            log.msg('StorageClientService: have not found FuelSensora or FuelSensor.Index or FuelCounter or State.Adcs fields in "%s"' % (s,))
                    else:
                        if s.has_key('FuelSensor'):
                            controller_status = 0
                            controller_id = s['EquipId'] & 0xffff
                            if s['FuelSensor']['Level'].has_key('1'):
                                level_1 = s['FuelSensor']['Level']['1']
                            else:
                                level_1 = 0
                            if s['FuelSensor']['Level'].has_key('2'):
                                level_2 = s['FuelSensor']['Level']['2']
                            else:
                                level_2 = 0
                            adc_1 = 0
                            adc_2 = 0
                            count_1 = 0
                            count_2 = 0
                            idx = 0
                            if s.has_key('Flags'):
                                if s['Flags'].has_key('Online'):
                                    if not s['Flags']['Online']:
                                        controller_status = controller_status | 0x08
                                if s["Flags"].has_key("IsRequest"):
                                    if s["Flags"]["IsRequest"]:
                                        controller_status = controller_status | 0x80
                            state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
                            serial = s['EquipId']
                            self.insertKDUTMeasures(serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, lambda x: self.insertKDUTMeasuresSuccess(x, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx), lambda x: self.insertKDUTMeasuresFailure(x, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx))
                        if s.has_key('TemperatureSensor'):
                            t_status = 0
                            f_status = 0
                            controller_status = 0
                            controller_id = s['EquipId'] & 0xffff
                            if s['TemperatureSensor'].has_key('1'):
                                t_1 = s['TemperatureSensor']['1']
                            else:
                                t_1 = 0
                            if s['TemperatureSensor'].has_key('2'):
                                t_2 = s['TemperatureSensor']['2']
                            else:
                                t_2 = 0
                            if s['TemperatureSensor'].has_key('3'):
                                t_3 = s['TemperatureSensor']['3']
                            else:
                                t_3 = 0
                            if s['TemperatureSensor'].has_key('4'):
                                t_4 = s['TemperatureSensor']['4']
                            else:
                                t_4 = 0
                            adc_1 = 0
                            adc_2 = 0
                            count_1 = 0
                            count_2 = 0
                            idx = 0
                            if s.has_key('Flags'):
                                if s['Flags'].has_key('Online'):
                                    if not s['Flags']['Online']:
                                        controller_status = controller_status | 0x40
                                if s["Flags"].has_key("IsRequest"):
                                    if s["Flags"]["IsRequest"]:
                                        controller_status = controller_status | 0x80
                                if s["Flags"].has_key("TemperatureBusError"):
                                    if s["Flags"]["TemperatureBusError"]:
                                        controller_status = controller_status | 0x02
                            state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
                            serial = s['EquipId']
                            self.insertKDTEMeasures(serial, state_time, snapshot_id, controller_id, controller_status, t_status, f_status, t_1, t_2, t3, t4, adc_1, adc_2, count_1, count_2, idx, lambda x: self.insertKDUTMeasuresSuccess(x, serial, state_time, snapshot_id, controller_id, controller_status, t_status, f_status, t_1, t_2, t_3, t_4, adc_1, adc_2, count_1, count_2, idx), lambda x: self.insertKDUTMeasuresFailure(x, serial, state_time, snapshot_id, controller_id, controller_status, t_status, f_status, t_1, t_2, t_3, t_4, adc_1, adc_2, count_1, count_2, idx))
                else:
                    log.msg('StorageClientService:error creating lineguard gateway fields from "%s"' % (s,))
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[Snapshots_Write]')

    def insertSnapshotFailure(self, failure, s):
        log.msg("StorageClientService:snapshot record failed to insert : %s" % (failure.getTraceback()))

    def insertCurrentSnapshot(self, s, snapshot_id, callback, errback):
        state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
        created_time = datetime.datetime.utcfromtimestamp(s['CreatedTime'])
        self.dbpool.runQuery('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentSnapshots_Write] @p_ShotGUID=%s,@p_GateCode=%s,@p_SourceCode=%s,@p_DestinationCode=%s,@p_ShotTime=%s,@p_ReceiveTime=%s,@p_Direction=%s,@p_Channel=%s,@p_Entity=%s,@p_SubEntity=%s,@p_ReTranslation=%s,@p_ShotID=%s;SELECT @error_code',
                 (s['GUID'],"GATE.LINEGUARD",'%d' % (s['EquipId'],),"",state_time,created_time,"RECEIVE","GPRS","DATA","INFO",0, snapshot_id)).addCallback(callback).addErrback(errback)

    def insertCurrentSnapshotSuccess(self, result, s, snapshot_id):
#        log.msg("StorageClientService:current snapshot record inserted, result : %r" % (result))
        if result != None:
            error_code = result[0][0]
            if 0 == error_code:
                log.msg('StorageClientService:error result executing [dbo].[CurrentSnapshots_Write]')
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[CurrentSnapshots_Write]')

    def insertCurrentSnapshotFailure(self, failure, s, snapshot_id):
        log.msg("StorageClientService:current snapshot record failed to insert : %s" % (failure.getTraceback()))

    def insertPosition(self, s, snapshot_id, lg_dict, callback, errback):
        if s.has_key('Location') and s.has_key('Index') and None != s['Index']:
            location_time = datetime.datetime.utcfromtimestamp(s['Location']['LocationTime'])
            self.dbpool.runQuery('DECLARE @position_id INT;EXECUTE @position_id=[dbo].[Positions_Write] @p_SourceCode=%s,@p_ObjectStatus=%s,@p_GSMStatus=%s,@p_GPSStatus=%s,@p_GPSTime=%s,@p_Latitude=%s,@p_Longitude=%s,@p_Direction=%s,@p_Speed=%s,@p_Flash=%s,@p_ShotID=%s;SELECT @position_id',
             ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],location_time,s['Location']['Latitude'],s['Location']['Longitude'],int(s['Location']['Course']/2.),int(s['Location']['Velocity']/1.85), s['Index'], snapshot_id)).addCallback(callback).addErrback(errback)

    def insertPositionSuccess(self, result, s, snapshot_id, lg_dict):
#        log.msg("StorageClientService:position record inserted, result : %r" % (result))
        if result != None:
            position_id = result[0][0]
            if 0 == position_id:
                log.msg('StorageClientService:error result executing [dbo].[Positions_Write]')
            else:
                self.inserted_n = self.inserted_n + 1
                if (lg_dict['status'] & 0x08) == 0:
                    self.insertCurrentPosition(s, snapshot_id, lg_dict, position_id, lambda x: self.insertCurrentPositionSuccess(x, s, snapshot_id, lg_dict, position_id), lambda x: self.insertCurrentPositionFailure(x, s, snapshot_id, lg_dict, position_id))
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[Positions_Write]')

    def insertPositionFailure(self, failure, s, snapshot_id, lg_dict):
        log.msg("StorageClientService:position record failed to insert : %s" % (failure.getTraceback()))

    def insertCurrentPosition(self, s, snapshot_id, lg_dict, position_id, callback, errback):
        location_time = datetime.datetime.utcfromtimestamp(s['Location']['LocationTime'])
        self.dbpool.runQuery('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentPositions_Write] @p_SourceCode=%s,@p_ObjectStatus=%s,@p_GSMStatus=%s,@p_GPSStatus=%s,@p_GPSTime=%s,@p_Latitude=%s,@p_Longitude=%s,@p_Direction=%s,@p_Speed=%s,@p_Flash=%s,@p_ShotID=%s,@p_PositionID=%s;SELECT @error_code',
             ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],location_time,s['Location']['Latitude'],s['Location']['Longitude'],int(s['Location']['Course']/2.),int(s['Location']['Velocity']/1.85), s['Index'], snapshot_id, position_id))

    def insertCurrentPositionSuccess(self, result, s, snapshot_id, lg_dict, position_id):
#        log.msg("StorageClientService:current position record inserted, result : %r" % (result))
        if result != None:
            error_code = result[0][0]
            if 0 == error_code:
                log.msg('StorageClientService:error result executing [dbo].[CurrentPositions_Write]')
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[CurrentPositions_Write]')

    def insertCurrentPositionFailure(self, failure, s, snapshot_id, lg_dict, position_id):
        log.msg("StorageClientService:current position record failed to insert : %s" % (failure.getTraceback()))

    def insertBaseMeasures(self, s, snapshot_id, lg_dict, callback, errback):
        if s.has_key('Index') and None != s['Index']:
            state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
            self.dbpool.runQuery('DECLARE @basemeasure_id INT;EXECUTE @basemeasure_id=[dbo].[BaseMeasures_Write2] %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d;SELECT @basemeasure_id;',
             ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],lg_dict['can_status'],lg_dict['power_status'],lg_dict['alarms_now'],lg_dict['alarms_old'],lg_dict['alarms'],lg_dict['alarms_was'],'%d' % (lg_dict['who'],),int(0.5 + lg_dict['adc1']*(1024 / 2.56)),int(0.5 + lg_dict['adc2']*(1024 / 2.56)),int(0.5 + lg_dict['adc3']*(1024 / 2.56)),int(0.5 + lg_dict['adc4']*(1024 / 2.56)),int(0.5 + lg_dict['power']*(1024 / 2.56)),int(0.5 + lg_dict['battery']*(1024 / 2.56)),lg_dict['outs'],0,s['Index'],state_time,snapshot_id)).addCallback(callback).addErrback(errback)

    def insertBaseMeasuresSuccess(self, result, s, snapshot_id, lg_dict):
#        log.msg("StorageClientService:basemeasure record inserted, result : %r" % (result))
        if result != None:
            basemeasure_id = result[0][0]
            if 0 == basemeasure_id:
                log.msg('StorageClientService:error result executing [dbo].[BaseMeasures_Write2]')
            else:
                self.inserted_n = self.inserted_n + 1
                if (lg_dict['status'] & 0x08) == 0:
                    self.insertCurrentMeasures(s, snapshot_id, lg_dict, basemeasure_id, lambda x: self.insertCurrentMeasuresSuccess(x, s, snapshot_id, lg_dict, basemeasure_id), lambda x: self.insertCurrentMeasuresFailure(x, s, snapshot_id, lg_dict, basemeasure_id))
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[BaseMeasures_Write2]')

    def insertBaseMeasuresFailure(self, failure, s, snapshot_id, lg_dict):
        log.msg("StorageClientService:basemeasure record failed to insert : %s" % (failure.getTraceback()))

    def insertCurrentMeasures(self, s, snapshot_id, lg_dict, basemeasure_id, callback, errback):
        state_time = datetime.datetime.utcfromtimestamp(s['StateTime'])
        self.dbpool.runQuery('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentMeasures_Write2] %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s;SELECT @error_code;',
         ('%d' % (s['EquipId'],),lg_dict['status'],lg_dict['gsm_status'],lg_dict['gps_status'],lg_dict['can_status'],lg_dict['power_status'],lg_dict['alarms_now'],lg_dict['alarms_old'],lg_dict['alarms'],lg_dict['alarms_was'],'%d' % (lg_dict['who'],),int(0.5 + lg_dict['adc1']*(1024 / 2.56)),int(0.5 + lg_dict['adc2']*(1024 / 2.56)),int(0.5 + lg_dict['adc3']*(1024 / 2.56)),int(0.5 + lg_dict['adc4']*(1024 / 2.56)),int(0.5 + lg_dict['power']*(1024 / 2.56)),int(0.5 + lg_dict['battery']*(1024 / 2.56)),lg_dict['outs'],0,s['Index'],state_time,snapshot_id,basemeasure_id)).addCallback(callback).addErrback(errback)

    def insertCurrentMeasuresSuccess(self, result, s, snapshot_id, lg_dict, measure_id):
#        log.msg("StorageClientService:current measure record inserted, result : %r" % (result))
        if result != None:
            error_code = result[0][0]
            if 0 == error_code:
                log.msg('StorageClientService:error result executing [dbo].[CurrentMeasures_Write2]')
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[CurrentMeasures_Write2]')

    def insertCurrentMeasuresFailure(self, failure, s, snapshot_id, lg_dict, measure_id):
        log.msg("StorageClientService:current measure record failed to insert : %s" % (failure.getTraceback()))

    def insertKDUTMeasures(self, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, callback, errback):
        self.dbpool.runQuery('DECLARE @kdutmeasure_id INT;EXECUTE @kdutmeasure_id=[dbo].[KDUTMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDutSerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_LLS1=%s,@p_LLS2=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s;SELECT @kdutmeasure_id',
                 (snapshot_id,'%d'%(serial,),controller_id,state_time,controller_status,level_1,level_2,adc_1,adc_2,count_1,count_2,idx)).addCallback(callback).addErrback(errback)

    def insertKDUTMeasuresSuccess(self, result, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx):
        if result != None:
            kdutmeasure_id = result[0][0]
            if 0 == kdutmeasure_id:
                log.msg('StorageClientService:error result executing [dbo].[KDUTMeasures_Write]')
            else:
                self.inserted_n = self.inserted_n + 1
                if (controller_status & 0x08) == 0:
                    self.insertCurrentKDUTMeasures(serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, kdutmeasure_id, lambda x: self.insertCurrentKDUTMeasuresSuccess(x, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, kdutmeasure_id), lambda x: self.insertCurrentKDUTMeasuresFailure(x, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, kdutmeasure_id))
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[KDUTMeasures_Write]')

    def insertKDUTMeasuresFailure(self, failure, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx):
        log.msg("StorageClientService:KDUT measure record failed to insert : %s" % (failure.getTraceback()))

    def insertCurrentKDUTMeasures(self, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, kdutmeasure_id, callback, errback):
        self.dbpool.runQuery('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentKDUTMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDutSerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_LLS1=%s,@p_LLS2=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s,@p_KDUTMeasureID=%s;SELECT @error_code',
         (snapshot_id,'%d'%(serial,),controller_id,state_time,controller_status,level_1,level_2,adc_1,adc_2,count_1,count_2,idx,kdutmeasure_id)).addCallback(callback).addErrback(errback)

    def insertCurrentKDUTMeasuresSuccess(self, result, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, kdutmeasure_id):
        if result != None:
            error_code = result[0][0]
            if 0 == error_code:
                log.msg('StorageClientService:error result executing [dbo].[CurrentKDUTMeasures_Write]')
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[CurrentKDUTMeasures_Write]')

    def insertCurrentKDUTMeasuresFailure(self, failure, serial, state_time, snapshot_id, controller_id, controller_status, level_1, level_2, adc_1, adc_2, count_1, count_2, idx, kdutmeasure_id):
        log.msg("StorageClientService:current KDUT measure record failed to insert : %s" % (failure.getTraceback()))

    def insertKDTEMeasures(serial, state_time, snapshot_id, controller_id, controller_status, t_status, f_status, t_1, t_2, t3, t4, adc_1, adc_2, count_1, count_2, idx, callback, errback):
        self.dbpool.runQuery('DECLARE @kdtemeasure_id INT;EXECUTE @kdtemeasure_id=[dbo].[KDTEMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDTESerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_TempStatus=%s,@p_FuelStatus=%s,@p_Temp_1=%s,@p_Temp_2=%s,@p_Temp_3=%s,@p_Temp_4=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s;SELECT @kdtemeasure_id',
                 (snapshot_id,'%d'%(serial,),controller_id,state_time,controller_status,t_status, f_status,t_1,t_2,t_3,t_4,adc_1,adc_2,count_1,count_2,idx)).addCallback(callback).addErrback(errback)

    def insertKDTEMeasuresSuccess(self, result, serial, state_time, snapshot_id, controller_id,  controller_status, t_status, f_status, t_1, t_2, t_3, t_4,adc_1, adc_2, count_1, count_2, idx):
        if result != None:
            kdtemeasure_id = result[0][0]
            if 0 == kdtemeasure_id:
                log.msg('StorageClientService:error result executing [dbo].[KDTEMeasures_Write]')
            else:
                self.inserted_n = self.inserted_n + 1
                if (controller_status & 0x40) == 0:
                    self.insertCurrentKDTEMeasures(serial, state_time, snapshot_id, controller_id,  controller_status, t_status, f_status, t_1, t_2, t_3, t_4,adc_1, adc_2, count_1, count_2, idx, kdtemeasure_id)
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[KDTEMeasures_Write]')

    def insertKDTEMeasuresFailure(self, failure, serial, state_time, snapshot_id, controller_id, controller_status, t_status, f_status, t_1, t_2, t_3, t_4, adc_1, adc_2, count_1, count_2, idx):
        log.msg("StorageClientService:KDTE measure record failed to insert : %s" % (failure.getTraceback()))
        
    def insertCurrentKDTEMeasures(self, serial, state_time, snapshot_id, controller_id,  controller_status, t_status, f_status, t_1, t_2, t_3, t_4,adc_1, adc_2, count_1, count_2, idx, kdtemeasure_id):
        self.dbpool.runQuery('DECLARE @error_code INT;EXECUTE @error_code=[dbo].[CurrentKDTEMeasures_Write] @p_ShotID=%s,@p_SourceCode=%s,@p_KDTESerial=%s,@p_EventTime=%s,@p_KDutStatus=%s,@p_TempStatus=%s,@p_FuelStatus=%s,@p_Temp_1=%s,@p_Temp_2=%s,@p_Temp_3=%s,@p_Temp_4=%s,@p_ADUT1=%s,@p_ADUT2=%s,@p_IInc=%s,@p_IDec=%s,@p_Flash=%s,@_KDTEMEasureID=%s;SELECT @error_code',
                 (snapshot_id,'%d'%(serial,),controller_id,state_time,controller_status,t_status, f_status,t_1,t_2,t_3,t_4,adc_1,adc_2,count_1,count_2,idx,kdtemeasure_id)).addCallback(callback).addErrback(errback)       
                 
    def insertCurrentKDTEMeasuresSuccess(self, serial, state_time, snapshot_id, controller_id,  controller_status, t_status, f_status, t_1, t_2, t_3, t_4,adc_1, adc_2, count_1, count_2, idx, kdtemeasure_id):
        if result != None:
            error_code = result[0][0]
            if 0 == error_code:
                log.msg('StorageClientService:error result executing [dbo].[CurrentKDTEMeasures_Write]')
        else:
            log.msg('StorageClientService:error getting result of executing [dbo].[CurrentKDTEMeasures_Write]')

    def insertCurrentKDUTMeasuresFailure(self, failure, serial, state_time, snapshot_id, controller_id,  controller_status, t_status, f_status, t_1, t_2, t_3, t_4,adc_1, adc_2, count_1, count_2, idx, kdtemeasure_id):
        log.msg("StorageClientService:current KDTE measure record failed to insert : %s" % (failure.getTraceback()))
                                     
    def getLocationTrack(self, serial, timestamp_from, timestamp_to):
        time_from = datetime.datetime.utcfromtimestamp(timestamp_from)
        time_to = datetime.datetime.utcfromtimestamp(timestamp_to)
        d = defer.Deferred()
        def _gotResult(result, d):
            records = []
            for row in result:
                records.append ({'Time':time.mktime(row[4].timetuple()), 'Longitude':row[6], 'Latitude':row[5], 'Velocity':row[8] * 1.85, 'Course':2 * row[7], 'Online':(row[1] & 0x08) == 0})
            d.callback(records)
        def _gotFailure(failure, d):
            d.errback(Exception('error executing [dbo].[Positions_SelectPeriod]'))                 
        self.dbpool.runQuery('EXEC [dbo].[Positions_SelectPeriod] %s,%s,%s', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(lambda x:_gotFailure(x, d))
        return d

    def registerSource(self, serial):
        d = defer.Deferred()
        def _gotFailure(failure, d):
            log.msg("StorageClientService: error executing dbo].[Sources_EngineRegistration]: %s" % (failure.getTraceback()))
            d.errback(Exception('error executing [dbo].[Sources_EngineRegistration]'))                 
        self.dbpool.runOperation('EXEC [dbo].[Sources_EngineRegistration] %s', (serial,)).addCallback(d.callback).addErrback(lambda x:_gotFailure(x, d))
        return d
        
    def enableSource(self, serial):
        return self.registerSource(serial)

    def disableSource(self, serial):
        d = defer.Deferred()
        def _gotFailure(failure, d):
            log.msg("StorageClientService: error executing [dbo].[Sources_Disable]: %s" % (failure.getTraceback()))
            d.errback(Exception('error executing [dbo].[Sources_Disable]'))                 
        self.dbpool.runOperation('EXEC [dbo].[Sources_Disable] %s', (serial,)).addCallback(d.callback).addErrback(lambda x:_gotFailure(x, d))
        return d

    def getCounters(self):
        return {'Inserted':self.inserted_n,'Logged':self.logged_n}
