from twisted.internet import reactor
from twisted.application import service
from twisted.python import log
from twisted.spread import pb
from twisted.internet import task

import time

class MessageClientService(service.Service, pb.Referenceable):
    def __init__(self, h, p):
        self.root = None
        self.m = None        
        self.step = 5
        self.h = h
        self.p = p
        
    def onRootDisconnect(self, root):
        log.msg("MessageClientService: root disconnected")                
        self.root = None
        self.m = None
        if not hasattr(self, '_loop'):
            self._loop = task.LoopingCall(self.pulse)
            self._loop.start(self.step, now=False).addErrback(self._failed) 
                        
    def getRootObjectSuccess(self, root):
        self.root = root
        self.root.notifyOnDisconnect(self.onRootDisconnect)
        self.root.callRemote("Advise", self).addCallback(self.adviseSuccess).addErrback(self.adviseError)
        
    def getRootObjectError(self, reason):
        log.msg("MessageClientService: root object getting error: %s" % (reason))                
        if not hasattr(self, '_loop'):
            self._loop = task.LoopingCall(self.pulse)
            self._loop.start(self.step, now=False).addErrback(self._failed)   
                 
    def adviseSuccess(self, m):
        self.m = m   
        log.msg('MessageClientService: advise success')
        if hasattr(self, '_loop'):
            self._loop.stop()
            delattr(self, '_loop') 
                      
    def adviseError(self, reason):
        log.msg("MessageClientService: advise error: %s" % (reason))                
        if not hasattr(self, '_loop'):
            self._loop = task.LoopingCall(self.pulse)
            self._loop.start(self.step, now=False).addErrback(self._failed)
            
    def unadviseSuccess(self):
        log.msg('MessageClientService: unadvise success')
        
    def unadviseError(self, reason):
        log.msg("MessageClientService: unadvise error: %s" % (reason))
                                
    def _failed(self, why):
        self._loop.running = False
        log.err('MessageClientService: timer service failed:%s' % (why))
        
    def pulse(self):
        if None != self.root:
            if None == self.m:
                self.root.callRemote("Advise", self).addCallback(self.adviseSuccess).addErrback(self.adviseError)   
        else:
            cf = pb.PBClientFactory()
            cf.getRootObject().addCallback(self.getRootObjectSuccess).addErrback(self.getRootObjectError)                            
            reactor.connectTCP(self.h, self.p, cf)
            
    def startService(self):
        service.Service.startService(self)
        cf = pb.PBClientFactory()
        cf.getRootObject().addCallback(self.getRootObjectSuccess).addErrback(self.getRootObjectError)                            
        reactor.connectTCP(self.h, self.p, cf)
        
    def stopService(self):
        if hasattr(self, '_loop'):
            if self._loop.running:
                self._loop.stop()
        if None != self.root:
            if None != self.m:
                self.root.callRemote("Unadvise", self.m).addCallback(self.unadviseSuccess).addErrback(self.unadviseError)   
        return service.Service.stopService(self)
        
    def messageReceived(self, serial, data):
        pass
        
    def remote_messageReceived(self, serial, data):
        self.messageReceived(serial, data)

class MessageLogStateListener:
    def __init__(self, serial):
        self.serial = serial
    def stateChanged(self, state):
        if OneShotMessagePbClient.CONNECT_FAILURE == state:
            log.msg('MessageLogStateListener:%d communication error' % (self.serial))
        elif OneShotMessagePbClient.MESSAGE_SENDED == state:
            log.msg('MessageLogStateListener:%d message sended' % (self.serial))
        elif OneShotMessagePbClient.QUEUE_FAILURE == state:
            log.msg('MessageLogStateListener:%d communication error' % (self.serial))
            
class OneShotMessagePbClient:
    CONNECT_FAILURE = 1
    QUEUE_FAILURE = 2
    MESSAGE_SENDED = 3
    
    def __init__(self, mss_host, mss_port, serial, message, listener):
        self.mss_host = mss_host
        self.mss_port = mss_port
        self.root = None
        self.serial = serial
        self.message = message
        self.listener = listener
        self.step = 3
        self.running = None
        self.cmd_result = None
        self.estimated_end_time = None
        
    def run(self):
        self.connect()
        
    def connect(self):
        self.client_factory = pb.PBClientFactory()
        reactor.connectTCP(self.mss_host, self.mss_port, self.client_factory)
        d = self.client_factory.getRootObject()
        d.addCallback(self.onConnectSuccess).addErrback(self.onConnectFailure)
        
    def onConnectSuccess(self, result):
        self.root = result      
        d = self.root.callRemote('queueMessage', self.serial, self.message)
        d.addCallback(self.onQueueMessageSuccess).addErrback(self.onQueueMessageFailure)  
            
    def onConnectFailure(self, failure):        
        self.listener.stateChanged(self.CONNECT_FAILURE)
        
    def onQueueMessageSuccess(self, result):        
        self.listener.stateChanged(self.MESSAGE_SENDED)   
                     
    def onQueueMessageFailure(self, failure):
        log.msg('OneShotMessagePbClient:onQueueMessageFailure %s' % (failure))
        self.listener.stateChanged(self.QUEUE_FAILURE)




