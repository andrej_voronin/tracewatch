from twisted.spread import pb
from twisted.internet import reactor,task
from twisted.python import log

import time

class CommandQueueLogStateListener:
    def __init__(self, equip_id):
        self.equip_id = equip_id
    def stateChanged(self, state):
        if OneShotCommandQueuePbClient.ADD_COMMAND_TRUE == state:
            log.msg('CommandQueueStateListener:%d executing command' % (self.equip_id))
        elif OneShotCommandQueuePbClient.ADD_COMMAND_FALSE == state:
            log.msg('CommandQueueStateListener:%d equipment offline' % (self.equip_id))
        elif OneShotCommandQueuePbClient.ADD_COMMAND_FAILURE == state:
            log.msg('CommandQueueStateListener:%d communication error' % (self.equip_id))
        elif OneShotCommandQueuePbClient.GET_COMMAND_FAILURE == state:
            log.msg('CommandQueueStateListener:%d communication error' % (self.equip_id))
        elif OneShotCommandQueuePbClient.COMMAND_RUNNING == state:
            log.msg('CommandQueueStateListener:%d equipment busy' % (self.equip_id))
        elif OneShotCommandQueuePbClient.DELETE_COMMAND_FAILURE == state:
            log.msg('CommandQueueStateListener:%d communication error' % (self.equip_id))
        elif OneShotCommandQueuePbClient.CONNECT_FAILURE == state:
            log.msg('CommandQueueStateListener:%d communication error' % (self.equip_id))
        elif OneShotCommandQueuePbClient.COMMAND_STOPPED_SUCCESS == state:
            log.msg('CommandQueueStateListener:%d command SUCCESS' % (self.equip_id))
        elif OneShotCommandQueuePbClient.COMMAND_STOPPED_FAILURE == state:
            log.msg('CommandQueueStateListener:%d command FAILURE' % (self.equip_id))
        elif OneShotCommandQueuePbClient.TIMEOUT == state:
            log.msg('CommandQueueStateListener:%d command TIMEOUT' % (self.equip_id))
        elif OneShotCommandQueuePbClient.COMMAND_DELETED == state:
            log.msg('CommandQueueStateListener:%d command DELETED' % (self.equip_id))

class OneShotCommandQueuePbClient:
    CONNECT_FAILURE = 1
    DELETE_COMMAND_FAILURE = 2
    GET_COMMAND_FAILURE = 3
    ADD_COMMAND_FAILURE = 4
    ADD_COMMAND_TRUE = 5
    ADD_COMMAND_FALSE = 6
    COMMAND_RUNNING = 7
    COMMAND_STOPPED_SUCCESS = 8
    COMMAND_STOPPED_FAILURE = 9
    TIMEOUT = 10
    COMMAND_DELETED = 11
    def __init__(self, queue_host, queue_port, equip_id, cmd, listener):
        self.queue_host = queue_host
        self.queue_port = queue_port
        self.root = None
        self.equip_id = equip_id
        self.cmd = cmd
        self.listener = listener
        self.step = 3
        self.running = None
        self.cmd_result = None
        self.estimated_end_time = None
    def run(self):
        self.connect()
    def connect(self):
        self.client_factory = pb.PBClientFactory()
        reactor.connectTCP(self.queue_host, self.queue_port, self.client_factory)
        d = self.client_factory.getRootObject()
        d.addCallback(self.onConnectSuccess).addErrback(self.onConnectFailure)
    def onConnectSuccess(self, result):
        self.root = result      
        d = self.root.callRemote('getCommandStateByEquip', self.equip_id)
        d.addCallback(self.onGetCommandStateSuccess).addErrback(self.onGetCommandStateFailure)      
    def onConnectFailure(self, failure):        
        self.listener.stateChanged(self.CONNECT_FAILURE)
    def onDelCommandSuccess(self, result):
        d = self.root.callRemote('addCommandByEquip', self.equip_id, self.cmd)
        d.addCallback(self.onAddcommandSuccess).addErrback(self.onAddcommandFailure)
    def onDelCommandFailure(self, failure):
        self.listener.stateChanged(self.DELETE_COMMAND_FAILURE)
    def onGetCommandStateSuccess(self, result):
        if result['Result']:
            if result['State']['Running']:
                self.listener.stateChanged(self.COMMAND_RUNNING)                
            else:
                d = self.root.callRemote('delCommandByEquip', self.equip_id)
                d.addCallback(self.onDelCommandSuccess).addErrback(self.onDelCommandFailure)
        else:
            d = self.root.callRemote('addCommandByEquip', self.equip_id, self.cmd)
            d.addCallback(self.onAddcommandSuccess).addErrback(self.onAddcommandFailure)            
    def onGetCommandStateFailure(self, failure):
        log.msg('OneShotCommandQueuePbClient:onGetCommandStateFailure %s' % (failure))
        self.listener.stateChanged(self.GET_COMMAND_FAILURE)
    def onAddcommandSuccess(self, result):
        if result:
            self.listener.stateChanged(self.ADD_COMMAND_TRUE)
            self._loop = task.LoopingCall(self.pulse)
            self._loop.start(self.step, now=False).addErrback(self._failed)         
        else:
            self.listener.stateChanged(self.ADD_COMMAND_FALSE)                      
    def onAddcommandFailure(self, failure):
        self.listener.stateChanged(self.ADD_COMMAND_FAILURE)            
    def _failed(self, why):
        self._loop.running = False        
    def pulse(self):
        #if None != self.estimated_end_time:
            #now = int(time.time())
            #print now, self.estimated_end_time + self.step
            #if now > self.estimated_end_time + self.step:
                #self._loop.stop()
                #self.client_factory.disconnect()
                #self.listener.stateChanged(self.TIMEOUT)                                                        
        if None != self.running:
            if self.running:
                d = self.root.callRemote('getCommandStateByEquip', self.equip_id)
                d.addCallback(self.onGetRunningCommandStateSuccess).addErrback(self.onGetRunningCommandStateFailure)        
        else:
            d = self.root.callRemote('getCommandStateByEquip', self.equip_id)
            d.addCallback(self.onGetRunningCommandStateSuccess).addErrback(self.onGetRunningCommandStateFailure)                            
    def onGetRunningCommandStateSuccess(self, result):
        if result['Result']:
            if None == self.estimated_end_time:
                self.estimated_end_time = result['State']['EstimatedEndTime']
            self.running = result['State']['Running']
            if not self.running:
                if result['State']['Error']:
                    self.cmd_result = self.COMMAND_STOPPED_FAILURE
                else:
                    self.cmd_result = self.COMMAND_STOPPED_SUCCESS
                #self.client_factory.disconnect()                    
                #self.listener.stateChanged(self.cmd_result)
                d = self.root.callRemote('delCommandByEquip', self.equip_id)
                d.addCallback(self.onDelRunningCommandSuccess).addErrback(self.onDelRunningCommandFailure)                
        else:
                self.client_factory.disconnect()
                self.listener.stateChanged(self.COMMAND_DELETED)                             
    def onGetRunningCommandStateFailure(self, failure):
        self.listener.stateChanged(self.GET_COMMAND_FAILURE)                        
    def onDelRunningCommandSuccess(self, result):
        self.client_factory.disconnect()
        self.listener.stateChanged(self.cmd_result)                
    def onDelRunningCommandFailure(self, failure):
        self.client_factory.disconnect()
        self.listener.stateChanged(self.DELETE_COMMAND_FAILURE)
        self.listener.stateChanged(self.cmd_result)                                

