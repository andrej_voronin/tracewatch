from twisted.python import log

from frontend_client import FrontendClientService

class ExportClientService(FrontendClientService):
    create_exprs = ['''CREATE TABLE EXPORT_QUEUE (
    ID INTEGER PRIMARY KEY,
    EQUIP_ID INTEGER NOT NULL,
    CREATED_TIMESTAMP FLOAT NOT NULL,
    MESSAGE TEXT NOT NULL
    )
    ''',
    '''
    CREATE INDEX EXPORT_QUEUE_BY_EQUIP_ID_CREATED_TIMESTAMP ON EXPORT_QUEUE(EQUIP_ID, CREATED_TIMESTAMP)
    '''
    ]
    def createTables(self, dbcon):
        c = dbcon.cursor()
        for e in self.create_exprs:
            c.execute(e)
        c.close()
    def __init__(self, h, p, dbpool):
        FrontendClientService.__init__(self, h, p)
        self.dbpool = dbpool
    def insertSuccess(self, result):
#        log.msg("ExportClientService:Export record inserted, result : %r" % (result))
        pass
    def insertFailure(self, failure):
        log.msg("ExportClientService:Export record failed to insert : %s" % (failure.getTraceback()))
    def stateReport(self, serial, ts, j):
        if self.dbpool.dbapi.paramstyle == 'format':
            self.dbpool.runOperation("INSERT INTO EXPORT_QUEUE(EQUIP_ID,CREATED_TIMESTAMP,MESSAGE) VALUES(%s,%s,%s)", (serial, ts, j)).addCallback(self.insertSuccess).addErrback(self.insertFailure)
        elif self.dbpool.dbapi.paramstyle == 'qmark':
            self.dbpool.runOperation("INSERT INTO EXPORT_QUEUE(EQUIP_ID,CREATED_TIMESTAMP,MESSAGE) VALUES(?,?,?)", (serial, ts, j)).addCallback(self.insertSuccess).addErrback(self.insertFailure)
        elif self.dbpool.dbapi.paramstyle == 'pyformat':
            self.dbpool.runOperation("INSERT INTO EXPORT_QUEUE(EQUIP_ID,CREATED_TIMESTAMP,MESSAGE) VALUES(%(serial)s,%(ts)s,%(message)s)", {'serial':serial, 'ts':ts, 'message':j}).addCallback(self.insertSuccess).addErrback(self.insertFailure)
        elif self.dbpool.dbapi.paramstyle == 'numeric':
            self.dbpool.runOperation("INSERT INTO EXPORT_QUEUE(EQUIP_ID,CREATED_TIMESTAMP,MESSAGE) VALUES(:1,:2,:3)", (serial, ts, j)).addCallback(self.insertSuccess).addErrback(self.insertFailure)
        elif self.dbpool.dbapi.paramstyle == 'named':
            self.dbpool.runOperation("INSERT INTO EXPORT_QUEUE(EQUIP_ID,CREATED_TIMESTAMP,MESSAGE) VALUES(:serial,:ts,:message)", {'serial':serial, 'ts':ts, 'message':j}).addCallback(self.insertSuccess).addErrback(self.insertFailure)          
        else:
            log.msg("ExportClientService:unknown paramstyle %s for dbapi %s" % (self.dbpool.dbapi.paramstyle,self.dbpool.dbapiName))        
