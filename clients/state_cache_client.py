from twisted.python import log
from twisted.spread import pb
from twisted.web import xmlrpc
from twisted.internet import defer

try:
    import json
except ImportError:
    import simplejson as json

from frontend_client import FrontendClientService

class StateCacheService(FrontendClientService):
    def __init__(self, h, p):
        FrontendClientService.__init__(self, h, p, sc_r)
        self.loc_cache = {}
        self.cache = {}
        self.sc_r = sc_r
        
    def stateReport(self, serial, ts, j):
        msg = json.loads(j)
        if msg.has_key('Flags'):
            if msg['Flags'].has_key('Online'):
                if msg['Flags']['Online']:
                    if msg.has_key("StateTime"):
                        if msg.has_key("Location"):
                            if self.loc_cache.has_key(serial):
                                if msg['Location']['LocationTime'] != self.loc_cache[serial]['Location']['LocationTime']:
                                    if (abs(msg['Location']['Latitude'] - self.loc_cache[serial]['Location']['Latitude']) > 0.00001) or (abs(msg['Location']['Longitude'] - self.loc_cache[serial]['Location']['Longitude']) > 0.00001):
                                        self.firePositionChangedEvent(serial, msg['Location'])
                            else:
                                self.firePositionChangedEvent(serial, msg['Location'])
                            self.loc_cache[serial] = msg
                        self.cache[serial] = msg
                        
    def getLocation(self, serial):
        d = Deferred()
        if self.loc_cache.has_key(serial):
            unix_loc_date_time = self.loc_cache[serial]['Location']['LocationTime']
            longitude = self.loc_cache[serial]['Location']['Longitude']
            latitude = self.loc_cache[serial]['Location']['Latitude']
            course = self.loc_cache[serial]['Location']['Course']
            speed = self.loc_cache[serial]['Location']['Velocity']
            unix_state_date_time  = self.loc_cache[serial]['StateTime']
            d.callback({'DateTime':unix_loc_date_time, 'Longitude':longitude, 'Latitude':latitude, 'Course':course, 'Velocity':speed, 'StateDateTime':unix_state_date_time})
        else:
            d.errback('No information')
        return d
        
    def firePositionChangedEvent(self, serial, position):
        self.sc_r.notifyPositionChanged(self, serial, position)

class StateCachePbResource(pb.Root):
    def __init__(self, imp):
        self.imp = imp
        self.updateSinks = {}
        self.updateSinksMagic = 0
        
    def onUpdateSinkSuccess(self, result):
        pass
        
    def onUpdateSinkFailure(self, reason):
        log.msg('StateCachePbResource:update sink error "%s"' % (reason.getErrorMessage()))
        
    def notifyPositionChanged(self, serial, position):
        j = json.dumps(position)
        for m, updateSink in self.updateSinks.items():
            try:
                updateSink.callRemote("positionChanged", serial, j).addCallback(self.onUpdateSinkSuccess).addErrback(self.onUpdateSinkFailure)
            except pb.PBConnectionLost:
                log.msg('StateCachePbResource:perspective broker connection lost')
                self.unadvise(m)
            except pb.DeadReferenceError:
                log.msg('StateCachePbResource:dead reference error')
                self.unadvise(m)
                
    def advise(self, iface):
        self.updateSinksMagic = self.updateSinksMagic + 1
        self.updateSinks[self.updateSinksMagic] = iface
        log.msg('StateCachePbResource:%d sinks' % (len(self.updateSinks)))
        return self.updateSinksMagic
        
    def unadvise(self, m):
        del self.updateSinks[m]
        return True
        
    def remote_Advise(self, nf):
        return self.advise(nf)
        
    def remote_Unadvise(self, m):
        return self.unadvise(m)
        
    def remote_getLocation(self, equip_id):
        return self.imp.getLocation(equip_id)
        
class StateCacheXmlrpcResource(xmlrpc.XMLRPC):
    def __init__(self, imp):
        xmlrpc.XMLRPC.__init__(self)
        self.imp = imp
        
    def xmlrpc_getLocation(self, equip_id):
        return self.imp.getLocation(equip_id)



