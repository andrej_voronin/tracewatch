from twisted.python import log
from twisted.internet import defer, task

import datetime
import time
import calendar
import sqlite3
import cPickle
import os
import types
import uuid
import math

try:
    import json
except ImportError:
    import simplejson as json

import geojson

from frontend_client import FrontendClientService
from lineguard.services.lineguard import extractLineguardFields


def big_circle_distance(long_from, lat_from, long_to, lat_to):
    e_r = 6372795.

    r_long_from = math.radians(long_from)
    r_lat_from  = math.radians(lat_from)
    r_long_to   = math.radians(long_to)
    r_lat_to  = math.radians(lat_to)
    
    ts1 = math.sin((r_lat_to - r_lat_from) / 2) 
    ts2 = math.sin(math.fabs(r_long_to - r_long_from) / 2)
    d = 2 * math.asin (math.sqrt ((ts1 * ts1 + math.cos (r_lat_from) * math.cos (r_lat_to) * ts2 * ts2)))
    
    return e_r * d  

def duration(ts1, ts2):
    return '0:00:00:00'
    
def createTables(dbcon):
    create_exprs = ['''CREATE TABLE POSITION (
    OBJID INT(10) UNSIGNED NOT NULL,
    STATE_DATE_TIME DATETIME NOT NULL,
    CREATED_DATE_TIME DATETIME NOT NULL,
    LOC_DATE_TIME DATETIME NOT NULL,
    STATE TINYINT(3) UNSIGNED NOT NULL,
    GPS_STATE TINYINT(3) UNSIGNED NOT NULL,
    ALARMS_OLD SMALLINT(5) UNSIGNED NOT NULL,
    ALARMS SMALLINT(5) UNSIGNED NOT NULL,
    ALARMS_NOW SMALLINT(5) UNSIGNED NOT NULL,
    ALARMS_WAS SMALLINT(5) UNSIGNED NOT NULL,
    OUTS TINYINT(3) UNSIGNED NOT NULL,
    WHO TINYINT(3) UNSIGNED NOT NULL,
    LATITUDE FLOAT(8,6) NOT NULL,
    LONGITUDE FLOAT(9,6) NOT NULL,
    COURSE SMALLINT(3) UNSIGNED NOT NULL,
    SPEED SMALLINT(3) UNSIGNED NOT NULL,
    ADC1 FLOAT(3,1)  NOT NULL,
    ADC2 FLOAT(3,1)  NOT NULL,
    ADC3 FLOAT(3,1)  NOT NULL,
    ADC4 FLOAT(3,1)  NOT NULL,
    FLASH_ADDR INT(10) UNSIGNED NOT NULL
    )
    ''',
    '''
    CREATE INDEX POSITION_BY_CREATED_DATE_TIME ON POSITION (CREATED_DATE_TIME)
    ''',
    '''
    CREATE INDEX POSITION_BY_OBJID_LOC_DATE_TIME ON POSITION (OBJID, LOC_DATE_TIME)
    ''',
    '''
    CREATE INDEX POSITION_BY_OBJID_STATE_DATE_TIME ON POSITION (OBJID, STATE_DATE_TIME)
    '''
    ]
    c = dbcon.cursor()
    for e in create_exprs:
        c.execute(e)
    dbcon.commit()
    c.close()
    
def importData(dbcon, f):    
    cur = dbcon.cursor()
    imported_n = 0
    l = f.readline()
    l = l.strip('\r\n;')
    while None != l and 0 != len(l):
        cur.execute(l)
        imported_n = imported_n + 1
        l = f.readline()
        l = l.strip('\r\n;')
    dbcon.commit()
    cur.close()
    return imported_n    
    
def purgeData(dbcon, equip_ids, time_from, time_to):
    cur = dbcon.cursor()
    if None == equip_ids:
        if time_from == None:
            if time_to != None:
                where_expr = 'STATE_DATE_TIME < %s'
                cur.execute('DELETE FROM POSITION WHERE ' + where_expr, (time_to))
                dbcon.commit()
                return cur.rowcount
        else:
            if time_to == None:
                where_expr = 'STATE_DATE_TIME >= %s'
                cur.execute('DELETE FROM POSITION WHERE ' + where_expr, (time_from))
                dbcon.commit()
                return cur.rowcount    
            else:
                where_expr = 'STATE_DATE_TIME >= %s AND STATE_DATE_TIME < %s'
                cur.execute('DELETE FROM POSITION WHERE ' + where_expr, (time_from, time_to))
                dbcon.commit()
                return cur.rowcount
    else:
        if time_from == None:
            if time_to != None:
                where_expr = 'STATE_DATE_TIME < %s AND OBJID IN (%s'
                params = [time_to, equip_ids[0]]
                for equip_id in equip_ids[1:]:
                    where_expr = where_expr + ',%s'
                    params.append(equip_id)
                where_expr = where_expr + ')'
                cur.execute('DELETE FROM POSITION WHERE ' + where_expr, params)
                dbcon.commit()
                return cur.rowcount
            else:
                return None
        else:
            if time_to == None:
                where_expr = 'STATE_DATE_TIME >= %s AND OBJID IN (%s'
                params = [time_from, equip_ids[0]]
                for equip_id in equip_ids[1:]:
                    where_expr = where_expr + ',%s'
                    params.append(equip_id)
                where_expr = where_expr + ')'
                cur.execute('DELETE FROM POSITION WHERE ' + where_expr, params)
                dbcon.commit()
                return cur.rowcount                    
            else:
                where_expr = 'STATE_DATE_TIME >= %s AND STATE_DATE_TIME < %s AND OBJID IN (%s'
                params = [time_from, time_to, equip_ids[0]]
                for equip_id in equip_ids[1:]:
                    where_expr = where_expr + ',%s'
                    params.append(equip_id)
                where_expr = where_expr + ')'
                cur.execute('DELETE FROM POSITION WHERE ' + where_expr, params)
                dbcon.commit()
                return cur.rowcount
                
def exportData(dbcon, f, equip_ids, time_from, time_to):
    cur = dbcon.cursor()
    exported_n = 0
    fields = 'OBJID, STATE_DATE_TIME, CREATED_DATE_TIME, LOC_DATE_TIME, STATE, GPS_STATE, ALARMS_OLD, ALARMS, ALARMS_NOW, ALARMS_WAS, OUTS, WHO, LATITUDE, LONGITUDE, COURSE, SPEED, ADC1, ADC2, ADC3, ADC4, FLASH_ADDR'
    field_fmts = '%d, "%s", "%s", "%s", %d, %d, %d, %d, %d, %d, %d, %d, %f, %f, %d, %f, %f, %f, %f, %f, %d'    
    if None == equip_ids:
        if time_from == None:
            if time_to != None:
                where_expr = 'STATE_DATE_TIME < %s'
                cur.execute('SELECT ' + fields + ' FROM POSITION WHERE ' + where_expr, (time_to))
                r = cur.fetchone()
                while None != r:
                    exported_n = exported_n + 1
                    field_values = field_fmts % r
                    e_s = 'INSERT INTO POSITION (' + fields + ') VALUES (' + field_values + ');'
                    f.write(e_s + '\n')
                    r = cur.fetchone()
                return exported_n
            else:
                return None
        else:
            if time_to == None:
                where_expr = 'STATE_DATE_TIME >= %s'
                cur.execute('SELECT ' + fields + ' FROM POSITION WHERE ' + where_expr, (time_from))
                r = cur.fetchone()
                while None != r:
                    exported_n = exported_n + 1
                    field_values = field_fmts % r
                    e_s = 'INSERT INTO POSITION (' + fields + ') VALUES (' + field_values + ');'
                    f.write(e_s + '\n')
                    r = cur.fetchone()
                return exported_n
            else:
                where_expr = 'STATE_DATE_TIME >= %s AND STATE_DATE_TIME < %s'
                cur.execute('SELECT ' + fields + ' FROM POSITION WHERE ' + where_expr, (time_from, time_to))
                r = cur.fetchone()
                while None != r:
                    exported_n = exported_n + 1
                    field_values = field_fmts % r
                    e_s = 'INSERT INTO POSITION (' + fields + ') VALUES (' + field_values + ');'
                    f.write(e_s + '\n')
                    r = cur.fetchone()
                return exported_n
    else:
        if time_from == None:
            if time_to != None:
                where_expr = 'STATE_DATE_TIME < %s AND OBJID IN (%s'
                params = [time_to, equip_ids[0]]
                for equip_id in equip_ids[1:]:
                    where_expr = where_expr + ',%s'
                    params.append(equip_id)
                where_expr = where_expr + ')'
                cur.execute('SELECT ' + fields + ' FROM POSITION WHERE ' + where_expr, params)
                r = cur.fetchone()
                while None != r:
                    exported_n = exported_n + 1
                    field_values = field_fmts % r
                    e_s = 'INSERT INTO POSITION (' + fields + ') VALUES (' + field_values + ');'
                    f.write(e_s + '\n')
                    r = cur.fetchone()
                return exported_n
            else:
                return None
        else:
            if time_to == None:
                where_expr = 'STATE_DATE_TIME >= %s AND OBJID IN (%s'
                params = [time_from, equip_ids[0]]
                for equip_id in equip_ids[1:]:
                    where_expr = where_expr + ',%s'
                    params.append(equip_id)
                where_expr = where_expr + ')'
                cur.execute('SELECT ' + fields + ' FROM POSITION WHERE ' + where_expr, params)
                r = cur.fetchone()
                while None != r:
                    exported_n = exported_n + 1
                    field_values = field_fmts % r
                    e_s = 'INSERT INTO POSITION (' + fields + ') VALUES (' + field_values + ');'
                    f.write(e_s + '\n')
                    r = cur.fetchone()
                return exported_n
            else:
                where_expr = 'STATE_DATE_TIME >= %s AND STATE_DATE_TIME < %s AND OBJID IN (%s'
                params = [time_from, time_to, equip_ids[0]]
                for equip_id in equip_ids[1:]:
                    where_expr = where_expr + ',%s'
                    params.append(equip_id)
                where_expr = where_expr + ')'
                cur.execute('SELECT ' + fields + ' FROM POSITION WHERE ' + where_expr, params)
                r = cur.fetchone()
                while None != r:
                    exported_n = exported_n + 1
                    field_values = field_fmts % r
                    e_s = 'INSERT INTO POSITION (' + fields + ') VALUES (' + field_values + ');'
                    f.write(e_s + '\n')
                    r = cur.fetchone()
                return exported_n

class MySQLStorageClientService(FrontendClientService):
    logged_n = 0L
    inserted_n = 0L
    processingLogDb = False
    process_n = 0L

    def __init__(self, h, p, dbpool, log_file = None):
        FrontendClientService.__init__(self, h, p)
        self.dbpool = dbpool
        self.log_file = log_file
        self.views = {}
        self.step = 60
        self.loop = None
        if None != self.log_file:
            if not os.access(self.log_file, os.F_OK):
                self.createLogDb()
            else:
                self.importLogDb()
                
    def loop_failed(self, why):
        self.loop.running = False
        log.err('MySQLStorageClientService: timer service failed:%s' % (why))
        
    max_view_access_time = 60 * 10    
    def pulse(self):
        now = time.time()
        for k in self.views.keys():
            if (self.views[k][6] + self.max_view_access_time) < now:
                log.err('MySQLStorageClientService: track view %s expired' % (k,))
                del self.views[k]
                
    def startService(self):                
        FrontendClientService.startService(self)
        self.loop = task.LoopingCall(self.pulse)
        self.loop.start(self.step, now=False).addErrback(self.loop_failed)      
        
    def stopService(self):
        if None != self.loop:
            if self.loop.running:
                self.loop.stop()        
        return FrontendClientService.stopService(self)      
        
    def importLogDb(self):
        tmp_con = sqlite3.connect(self.log_file)
        c = tmp_con.cursor()
        c.execute('SELECT COUNT(*) FROM LOG')
        r = c.fetchone();
        if None != r:
            self.logged_n = int(r[0])
        tmp_con.close()
        
    def createLogDb(self):
        tmp_con = sqlite3.connect(self.log_file)
        c = tmp_con.cursor()
        c.execute('CREATE TABLE LOG(ID INTEGER PRIMARY KEY, DATA TEXT)')
        tmp_con.close()
        
    def logInsertSuccess(self, result, log_id):
        self.process_n = self.process_n - 1
        if 0 == self.process_n:
            self.processingLogDb = False
        self.logged_n = self.logged_n - 1
        self.inserted_n = self.inserted_n + 1
        tmp_con = sqlite3.connect(self.log_file)
        c = tmp_con.cursor()
        c.execute('DELETE FROM LOG WHERE ID=?',(log_id,))
        tmp_con.commit()
        tmp_con.close()
        
    def logInsertFailure(self, failure):
        self.process_n = self.process_n - 1
        if 0 == self.process_n:
            self.processingLogDb = False
        log.msg("MySQLStorageClientService:logged record failed to insert : %s" % (failure.getTraceback()))
        
    def processLogDb(self):
        if None != self.log_file:
            if not self.processingLogDb:
                self.processingLogDb = True
                tmp_con = sqlite3.connect(self.log_file)
                c = tmp_con.cursor()
                c.execute('SELECT ID, DATA FROM LOG ORDER BY ID DESC')
                r = c.fetchone()
                self.process_n = 0
                while None != r:
                    if type(r[1]) == types.UnicodeType:
                        s = r[1].encode('ascii')
                    else:
                        s = r[1]
                    lg_fields, lg_dict = cPickle.loads(s)
                    log_id = r[0]
                    self.insert(lg_fields, lg_dict, lambda x : self.logInsertSuccess(x, log_id), self.logInsertFailure)
                    self.process_n = self.process_n + 1
                    r = c.fetchone()
                tmp_con.close()
                return True
            else:
                return False
        return False
        
    def insertSuccess(self, result):
#        log.msg("MySQLStorageClientService:record inserted, result : %r" % (result))
        self.inserted_n = self.inserted_n + 1
        
    def insertFailure(self, failure, lg_fields, lg_dict):
        log.msg("MySQLStorageClientService:record failed to insert : %s" % (failure.getTraceback()))
        if None != failure.check(self.dbpool.dbapi.OperationalError, self.dbpool.dbapi.InternalError):
            if None != self.log_file:
                tmp_con = sqlite3.connect(self.log_file)
                c = tmp_con.cursor()
                c.execute("INSERT INTO LOG(DATA) VALUES(?)",(cPickle.dumps((lg_fields, lg_dict)),))
                tmp_con.commit()
                tmp_con.close()
                self.logged_n = self.logged_n + 1
                
    def insert(self, lg_fields, lg_dict, callback_handler, errback_handler):
        self.dbpool.runOperation("INSERT INTO POSITION (OBJID, STATE_DATE_TIME, CREATED_DATE_TIME, LOC_DATE_TIME, STATE, GPS_STATE, ALARMS_OLD, ALARMS, ALARMS_NOW, ALARMS_WAS, OUTS, WHO, LATITUDE, LONGITUDE, COURSE, SPEED, ADC1, ADC2, ADC3, ADC4, FLASH_ADDR) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", lg_fields).addCallback(callback_handler).addErrback(errback_handler)
        
    def stateReport(self, serial, ts, j):
        s = json.loads(j)
        lg_fields, lg_dict = extractLineguardFields(s)
        if None != lg_fields:
            #lg_fields = (equip_id, request_str, unix_state_date_time, unix_created_date_time, unix_loc_date_time, status, gps_status, alarms_old, alarms, alarms_now, alarms_was, outs, who, latitude, longitude, course, speed, adcs[1], adcs[2], adcs[3], adcs[4], f_addr, power, battery, gsm_status, can_status, power_status)
            lg_fields = list(lg_fields[:22])
            lg_fields[2] = datetime.datetime.utcfromtimestamp(lg_fields[2])
            lg_fields[3] = datetime.datetime.utcfromtimestamp(lg_fields[3])
            lg_fields[4] = datetime.datetime.utcfromtimestamp(lg_fields[4])
            lg_dict['unix_state_date_time'] = lg_fields[2]
            lg_dict['unix_created_date_time'] = lg_fields[3]
            lg_dict['unix_loc_date_time'] = lg_fields[4]
            lg_fields.pop(1)            
            self.insert(lg_fields, lg_dict, self.insertSuccess, lambda x: self.insertFailure(x, lg_fields, lg_dict))
        else:
            log.msg('MySQLStorageClientService:error extracting lggw protocol fields from "%s"' % (j))
            
    def getLocationTrack(self, serial, timestamp_from, timestamp_to):
        time_from = datetime.datetime.utcfromtimestamp(timestamp_from)
        time_to = datetime.datetime.utcfromtimestamp(timestamp_to)
        d = defer.Deferred()
        def _gotResult(result, d):
            records = []
            for row in result:
				records.append ({'Time':int(calendar.timegm(row[5].timetuple())), 'Longitude':row[0], 'Latitude':row[1], 'Velocity':row[2], 'Course':row[3], 'Online':(row[4] & 0x08) == 0})
            d.callback(records)
        if self.dbpool.dbapi.paramstyle == 'format':
            self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME FROM POSITION WHERE OBJID=%s AND LOC_DATE_TIME BETWEEN %s AND %s ORDER BY LOC_DATE_TIME', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
        elif self.dbpool.dbapi.paramstyle == 'qmark':
            self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME FROM POSITION WHERE OBJID=? AND LOC_DATE_TIME BETWEEN ? AND ? ORDER BY LOC_DATE_TIME', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
        elif self.dbpool.dbapi.paramstyle == 'pyformat':
            self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME FROM POSITION WHERE OBJID=%(serial)s AND LOC_DATE_TIME BETWEEN %(time_from)s AND %(time_to)s ORDER BY LOC_DATE_TIME', {'serial':serial, 'time_from':time_from, 'time_to':time_to}).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
        elif self.dbpool.dbapi.paramstyle == 'numeric':
            self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME FROM POSITION WHERE OBJID=:1 AND LOC_DATE_TIME BETWEEN :2 AND :3 ORDER BY LOC_DATE_TIME', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
        elif self.dbpool.dbapi.paramstyle == 'named':
            self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME FROM POSITION WHERE OBJID=:serial AND LOC_DATE_TIME BETWEEN :time_from AND :time_to ORDER BY LOC_DATE_TIME', {'serial':serial, 'time_from':time_from, 'time_to':time_to}).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
        else:
            log.msg("MySQLStorageClientService:unknown paramstyle %s for dbapi %s" % (self.dbpool.dbapi.paramstyle,self.dbpool.dbapiName))
        return d
        
    view_page_size = 64        
    min_hole_time = datetime.timedelta(minutes=2)    
    def getTrackViewPage(self, view_id, page_n):
        def _gotResult(result, d):
            records = []                        
            n = 0
            
            #prev_lon = None
            #prev_lat = None
            #prev_ts = None           
            #prev_speed = None
                        
            #lines = []            
            for row in result:
                lon = row[0]
                lat = row[1]
                ts = row[6]
                speed = row[2]
                heading = row[3]
                if n < self.view_page_size:
                    records.append ({'Time':int(calendar.timegm(ts.timetuple())), 'LocationTime':int(calendar.timegm(row[5].timetuple())), 'Longitude':lon, 'Latitude':lat, 'Velocity':speed, 'Course':heading, 'Online':(row[4] & 0x08) == 0})
                #if (None != prev_lon) and (None != prev_lat):
                    #if (abs(prev_lon - lon) > 0.000001) or  (abs(prev_lat - lat) > 0.000001):                             
                        #if prev_ts != None:
                            #dist = big_circle_distance(prev_lon, prev_lat, lon, lat)
                            #dist = int(dist * 100) / 100.
                            #if (prev_ts + self.min_hole_time) > ts:                                
                                #lines.append(geojson.Feature(geometry=geojson.LineString([(prev_lon, prev_lat),(lon, lat)]),properties={'speed':prev_speed, 'date':prev_ts.strftime('%Y-%m-%d'), 'time':prev_ts.strftime('%H:%M:%S'), 'distance':dist, 'duration':duration(prev_ts, ts)}))
                            #else:                           
                                #lines.append(geojson.Feature(geometry=geojson.Point((prev_lon, prev_lat)),properties={'speed':prev_speed, 'date':prev_ts.strftime('%Y-%m-%d'), 'time':prev_ts.strftime('%H:%M:%S'), 'distance':dist, 'duration':duration(prev_ts, ts)}))                    
                #prev_lon = lon
                #prev_lat = lat
                #prev_ts = ts
                #prev_speed = speed
                n = n + 1
                                                            
            #mline = geojson.FeatureCollection(lines)
            #g_j = geojson.dumps(mline)            
            #view = {'Records':records, 'Line':g_j}    
            
            view = {'Records':records}    
            self.views[view_id][4][page_n] = view    
            d.callback(view)     
        if self.views.has_key(view_id):
            d = defer.Deferred()
            self.views[view_id][6] = time.time()
            if self.views[view_id][4].has_key(page_n):
                d.callback(self.views[view_id][4][page_n])          
            else:
                serial = self.views[view_id][0]
                time_from = self.views[view_id][1]
                time_to = self.views[view_id][2]
                offset = page_n * self.view_page_size
                limit = self.view_page_size + 1
                if self.dbpool.dbapi.paramstyle == 'format':
                    self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME, STATE_DATE_TIME FROM POSITION WHERE OBJID=%s AND STATE_DATE_TIME BETWEEN %s AND %s ORDER BY STATE_DATE_TIME  LIMIT %s OFFSET %s', (serial, time_from, time_to, limit, offset)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
                elif self.dbpool.dbapi.paramstyle == 'qmark':
                    self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME, STATE_DATE_TIME FROM POSITION WHERE OBJID=? AND STATE_DATE_TIME BETWEEN ? AND ? ORDER BY STATE_DATE_TIME LIMIT ? OFFSET ?', (serial, time_from, time_to, limit, offset)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
                elif self.dbpool.dbapi.paramstyle == 'pyformat':
                    self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME, STATE_DATE_TIME FROM POSITION WHERE OBJID=%(serial)s AND STATE_DATE_TIME BETWEEN %(time_from)s AND %(time_to)s ORDER BY STATE_DATE_TIME  LIMIT %(limit)s OFFSET %(offset)s', {'serial':serial, 'time_from':time_from, 'time_to':time_to, 'offset':offset, 'limit':limit}).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
                elif self.dbpool.dbapi.paramstyle == 'numeric':
                    self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME, STATE_DATE_TIME FROM POSITION WHERE OBJID=:1 AND STATE_DATE_TIME BETWEEN :2 AND :3 ORDER BY STATE_DATE_TIME  LIMIT :4 OFFSET :5', (serial, time_from, time_to, limit, offset)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
                elif self.dbpool.dbapi.paramstyle == 'named':
                    self.dbpool.runQuery('SELECT DISTINCT LONGITUDE, LATITUDE, SPEED, COURSE, STATE, LOC_DATE_TIME, STATE_DATE_TIME FROM POSITION WHERE OBJID=:serial AND STATE_DATE_TIME BETWEEN :time_from AND :time_to ORDER BY STATE_DATE_TIME LIMIT :limit OFFSET :offset', {'serial':serial, 'time_from':time_from, 'time_to':time_to, 'offset':offset, 'limit':limit}).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
                else:
                    log.msg("MySQLStorageClientService:unknown paramstyle %s for dbapi %s" % (self.dbpool.dbapi.paramstyle,self.dbpool.dbapiName))
            return d
        else:
            d = defer.Deferred()
            d.errback(KeyError())
            return d 
            
    def createTrackView(self, serial, timestamp_from, timestamp_to):
        time_from = datetime.datetime.utcfromtimestamp(timestamp_from)
        time_to = datetime.datetime.utcfromtimestamp(timestamp_to)
        def _gotResult(result, d):
            now = time.time()
            created_time_ts = now
            last_access_time_ts = now
            cnt = result[0][0]
            view_id = str(uuid.uuid4())
            pages = {}       
            self.views[view_id] = [serial, time_from, time_to, cnt, pages, created_time_ts, last_access_time_ts]
            d.callback({'ViewId':view_id, 'ViewRecordsCount':cnt, 'ViewPageSize':self.view_page_size})        
        f = None
        for k, v in self.views.iteritems():
            if v[0] == serial:
                if (v[1] == time_from) and (v[2] == time_to):
                    f = k
        d = defer.Deferred()                                
        if f == None:
            if self.dbpool.dbapi.paramstyle == 'format':
                self.dbpool.runQuery('SELECT COUNT(DISTINCT STATE_DATE_TIME) FROM POSITION WHERE OBJID=%s AND STATE_DATE_TIME BETWEEN %s AND %s', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
            elif self.dbpool.dbapi.paramstyle == 'qmark':
                self.dbpool.runQuery('SELECT COUNT(DISTINCT STATE_DATE_TIME) FROM POSITION WHERE OBJID=? AND STATE_DATE_TIME BETWEEN ? AND ?', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
            elif self.dbpool.dbapi.paramstyle == 'pyformat':
                self.dbpool.runQuery('SELECT COUNT(DISTINCT STATE_DATE_TIME) FROM POSITION WHERE OBJID=%(serial)s AND STATE_DATE_TIME BETWEEN %(time_from)s AND %(time_to)s', {'serial':serial, 'time_from':time_from, 'time_to':time_to}).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
            elif self.dbpool.dbapi.paramstyle == 'numeric':
                self.dbpool.runQuery('SELECT COUNT(DISTINCT STATE_DATE_TIME) FROM POSITION WHERE OBJID=:1 AND STATE_DATE_TIME BETWEEN :2 AND :3', (serial, time_from, time_to)).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
            elif self.dbpool.dbapi.paramstyle == 'named':
                self.dbpool.runQuery('SELECT COUNT(DISTINCT STATE_DATE_TIME) FROM POSITION WHERE OBJID=:serial AND STATE_DATE_TIME BETWEEN :time_from AND :time_to', {'serial':serial, 'time_from':time_from, 'time_to':time_to}).addCallback(lambda x:_gotResult(x, d)).addErrback(d.errback)
            else:
                log.msg("MySQLStorageClientService:unknown paramstyle %s for dbapi %s" % (self.dbpool.dbapi.paramstyle,self.dbpool.dbapiName))
            return d
        else:
            d.callback({'ViewId':f, 'ViewRecordsCount':self.views[f][3], 'ViewPageSize':self.view_page_size})
            return d 
               
    def getCounters(self):
        return {'Inserted':self.inserted_n,'Logged':self.logged_n}
        


